from cdnwakfu import recipe_construction, processItems, recipes_data
from data import latest
import json

def compareFile(target_folder:str, name_file:str):
    operande = None
    with open('%s/%s' % (target_folder, name_file)) as json_file:
        operande = json.load(json_file)
    with open('./building/%s' % name_file ) as json_file:
        return operande == json.load(json_file)
    return False

target_folder = "./tests"
name_file = "recipePattern.json"
recipe_construction(latest, recipes_data, target_folder, False)
assert True == compareFile(target_folder, name_file)
processItems(latest, target_folder)
assert True == compareFile(target_folder, "items.json")
assert True == compareFile(target_folder, "jobsItems.json")
assert True == compareFile(target_folder, "recipeResults.json")
assert True == compareFile(target_folder, "recipeIngredients.json")
assert True == compareFile(target_folder, "recipes.json")