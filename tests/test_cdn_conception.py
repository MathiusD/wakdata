import unittest
from cdnwakfu import *

class Test_CDNWakfu(unittest.TestCase):

    def test_Text(self):
        txt = Text({"fr":"Nlap","en":"Drup","es":"Glip","pt":"Brup"})
        try:
            nTxt = Text({"fr":"Nlap","sp":"Drup","es":"Glip","pt":"Brup"})
        except Exception as exc:
            self.assertTrue(exc.__class__ == AttributeError)
        self.assertEqual("Nlap", txt.fr)
        self.assertEqual("Drup", txt.__str__())
        self.assertEqual("Brup", txt.getByLang(pt))
        self.assertEqual("Glip", getattr(txt, "es"))
        self.assertEqual("Glip", txt.getByShortLang("sp"))
        self.assertEqual("Drup", txt.getByShortLang())
        self.assertEqual("Drup", txt.getByShortLang("123"))
        self.assertEqual(
            [
                "en",
                "fr",
                "sp",
                "pt"
            ],
            Text.shortLangs()
        )
        self.assertEqual(
            [
                "en",
                "fr",
                "es",
                "sp",
                "pt"
            ],
            Text.shortLangsAccepted()
        )

    def test_DataElement(self):
        data = DataElement()
        data._recursion(3)
        self.assertEqual(2, data._recursive)
        data._recursion(1)
        self.assertEqual(0, data._recursive)
        data._recursion(0)
        self.assertEqual(None, data._recursive)
        data._recursion(-1)
        self.assertEqual(None, data._recursive)
        data._recursion()
        self.assertEqual(None, data._recursive)

    def test_Rarity(self):
        rarity = Rarity.Commun
        self.assertEqual("Common", rarity.__str__())
        self.assertEqual("Commun", Rarity.RaritytoText(rarity.value).fr)

    def test_State(self):
        state = State({"definition":{"id":3},"title":{"fr":"Nlap","en":"Drup","es":"Glip","pt":"Brup"}})
        self.assertEqual("Drup", state.__str__())
        self.assertEqual(3, state.id)
        self.assertEqual("Nlap", state.title.fr)
        self.assertEqual("Glip", getattr(state.title, "es"))

    def test_RecipeCategorie(self):
        defcat = {
                "definition":
                {
                    "id":45,
                    "isArchive":False,
                    "isNoCraft":False,
                    "isHidden":False,
                    "xpFactor":45,
                    "isInnate":True
                },
                "title":
                {
                    "fr":"Nlap",
                    "en":"Broup",
                    "es":"Glip",
                    "pt":"Rboup"
                }
            }
        cat = RecipeCategorie(defcat)
        self.assertEqual("Broup", cat.__str__())
        self.assertEqual(RecipeCategorie.findRecipeCategorie([defcat], cat.id).__str__(), cat.__str__())

    def test_Action(self):
        defact1 = {"definition":{"id":9,"effect":"Bloup"},"description":{"fr":"[#1] Point{[>1]?s:} de Vie","en":"[#1] PV{[>1]?s:}","es":"Glip","pt":"Brup"}}
        defact2 = {"definition":{"id":9,"effect":"Bloup"}}
        act1 = Action(defact1)
        act2 = Action(defact2)
        defacts = [defact1, defact2]
        self.assertEqual("[#1] PV{[>1]?s:}", act1.__str__())
        self.assertEqual("Bloup", act2.__str__())
        self.assertEqual(act2.definition.id, Action.findAction(defacts, 9).definition.id)
        self.assertEqual(act2.definition.effect, Action.findAction(defacts, 9).definition.effect)
        self.assertEqual(None, Action.findAction(defacts, 45))
        self.assertEqual("220 Points de Vie", act1.adaptingDescription([0, 2, 2, 2], 110).fr)
        self.assertEqual("220 PVs", act1.adaptingDescription([0, 2, 2, 2], 110).en)
        self.assertEqual("Bloup", act2.adaptingDescription([0, 2, 2, 2], 110))

    def test_Effect(self):
        defeffect1 = {"definition":{"id":9,"actionId":9,"areaShape":45,"areaSize":2,"params":[0, 2, 2, 2]}}
        defeffect2 = {"definition":{"id":42,"actionId":42,"areaShape":45,"areaSize":2,"params":[0, 2, 2, 2]}}
        defeffect3 = {"definition":{"id":12,"actionId":45,"areaShape":45,"areaSize":2,"params":[0, 2, 2, 2]}}
        subeffect = [{"effect":defeffect1}, {"effect":defeffect3}]
        defact1 = {"definition":{"id":9,"effect":"Bloup"},"description":{"fr":"[#1] Point{[>1]?s:} de Vie","en":"Drup","es":"Glip","pt":"Brup"}}
        defact2 = {"definition":{"id":42,"effect":"REG"}}
        defact3 = {"definition":{"id":45,"effect":"Salut"}}
        data = {"actions":[defact1, defact2, defact3]}
        effect1 = Effect(defeffect1)
        effect2 = Effect(defeffect2, subeffect)
        effect3 = Effect(defeffect3)
        effect1.fetchData(data, 110)
        effect2.fetchData(data, 110)
        effect3.fetchData(data, 110)
        row_effect1 = {"data":[effect1.effects[0].fr], "prefix":"\t"}
        row_effect3 = {"data":[effect3.effects[0]], "prefix":"\t"}
        row_effect2 = {"data":[row_effect1, row_effect3], "prefix":""}
        self.assertEqual("9", effect1.__str__())
        self.assertEqual("42", effect2.__str__())
        self.assertEqual("12", effect3.__str__())
        self.assertEqual(row_effect2, effect2._row_sheet())

    def test_ResourceType(self):
        defrestype = {"definition":{"id":9, "affectWakfu":True}, "title":{"fr":"Nlap","en":"Drup","es":"Glip","pt":"Brup"}}
        defrestypebis = {"definition":{"id":10, "affectWakfu":True}, "title":{"fr":"Brip","en":"Drup","es":"Glip","pt":"Brup"}}
        deftypes = [defrestype, defrestypebis]
        typ = ResourceType(defrestype)
        typbis = ResourceType(defrestypebis)
        self.assertEqual(typ.__str__(), "Drup")
        self.assertEqual(typ.sheet("fr", "\t"), "\tNlap")
        self.assertEqual(typ.id, 9)
        self.assertEqual(typ.affectWakfu, True)
        self.assertEqual(typbis.__str__(), ResourceType.findResourceType(deftypes, 10).__str__())

    def test_Resource(self):
        data = {"resourceTypes":[{"definition":{"id":4, "affectWakfu":True}, "title":{"fr":"Glip","en":"Drup","es":"Glip","pt":"Brup"}}]}
        datares1 = {"definition":{
            "id":9,
            "resourceType":4,
            "isBlocking":True,
            "idealRainRangeMin":9,
            "idealRainRangeMax":45,
            "idealTemperatureRangeMin":2,
            "idealTemperatureRangeMax":45,
            "iconGfxId":542165,
            "lastEvolutionStep":5,
            "usableByHeroes":True,
            "idealRain":40
        },"title":{"fr":"Nlap","en":"Drup","es":"Glip","pt":"Brup"}}
        datares2 = {"definition":{
            "id":470,
            "resourceType":4,
            "isBlocking":True,
            "idealRainRangeMin":9,
            "idealRainRangeMax":45,
            "idealTemperatureRangeMin":2,
            "idealTemperatureRangeMax":45,
            "iconGfxId":542165,
            "lastEvolutionStep":5,
            "usableByHeroes":True,
            "idealRain":40
        },"title":{"fr":"Gouloup","en":"Drup","es":"Glip","pt":"Brup"}}
        dataress = [datares1, datares2]
        res1 = Resource(datares1)
        self.assertEqual(res1.__str__(), "Drup")
        self.assertEqual(None, res1.definition.resourceType)
        res1.fetchData(data)
        self.assertEqual("Drup", res1.definition.resourceType.__str__())
        self.assertEqual("Drup", Resource.findResource(dataress, 470).__str__())
