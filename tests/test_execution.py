from cdnwakfu import Recipe, Item, JobItem, Rarity, UserReadableElement, sheet_process, LevelCriteria, EquipementItemTypeCriteria, ActionCriteria, RarityCriteria, Rarity
from data import latest

def show(compo:UserReadableElement):
    if compo and isinstance(compo, UserReadableElement):
        compo.fetchData(latest)
        print(compo.sheet("fr","\t"))
    else:
        print(compo)

def showMany(compos:list):
    for compo in compos:
        show(compo)

def showResults(data:list, search:str):
    print("Pour %s:" % search)
    showMany(data)

def searchRecipes(search:str):
    data = Recipe.findRecipeByItemResultName(latest, search, "fr")
    showResults(data, search)

def searchItems(search:str):
    data = Item.findItemsByName(latest["items"], search, "fr")
    showResults(data, search)

def searchJobItems(search:str):
    data = JobItem.findItemsByName(latest["jobsItems"], search, "fr")
    showResults(data, search)

id = 1160
print("Pour l'id = %i:" % id)
recipe = Recipe.findRecipe(latest['recipes'], id)
show(recipe)
print("Rapporte %i xp pour un crafteur de niveau 45" % recipe.getXP(45, latest))
print("Avec un taux de réussite de %f pour un crafteur de niveau 36" % recipe.craftRate(36))
print("Avec un taux de réussite de %f pour un crafteur de niveau 40" % recipe.craftRate(40))
print("Avec un taux de réussite de %f pour un crafteur de niveau 45" % recipe.craftRate(45))
print("Avec un taux de réussite de %f pour un crafteur de niveau 30" % recipe.craftRate(30))
print("Avec un taux de réussite de %f pour un crafteur de niveau 50" % recipe.craftRate(50))
searchRecipes("Pain Elf'Hic")
searchRecipes("Cape Hilare")
searchRecipes("Tronchoneuse")
searchRecipes("Egreu-Visse à l'Escamèche")
searchRecipes("Epée Eter")
showResults(Recipe.findRecipeByItemResult(latest, Item.findItemsByName(latest["items"], "Epée de Bonta", "fr")[0]), "Epée de Bonta")
data = Recipe.findRecipeByItemResultName(latest, "Epée Eter", "fr")
if len(data) > 0:
    print("Tout les composants de l'Epée Eternelle :")
    dat = data[0].getAllComponents(latest)
    for compo in dat:
        compo.fetchData(latest, 1)
        print("\t%i - %s" % (compo.quantity, compo.item.title.fr))
id = 27717
print("Pour l'id = %i:" % id)
show(Item.findItem(latest['items'], id))
id = 2021
print("Pour l'id = %i:" % id)
show(Item.findItem(latest['items'], id))
searchItems("Pioche")
id = 1718
print("Pour l'id = %i:" % id)
show(JobItem.findItem(latest['jobsItems'], id))
searchJobItems("Cawotte")
showResults(Item.findItemsByActions(latest, [234, 166]), "L'action avec un Gain de Barda et de la sagesse")
showResults(Item.findItemsWithCriteria(latest, ActionCriteria(2001, requestValue=[30], requestOperator=[">="])), "Les Objets donnant un bonus de récolte de 30% ou supérieur")
assert len(Item.findItemsByAction(latest, 162)) == len(Item.findItemsByActions(latest, [162]))
showResults(Item.findItemsWithCriterias(latest, [EquipementItemTypeCriteria(134), ActionCriteria(2001), LevelCriteria(54, ">="), RarityCriteria(Rarity.Rare)]), "Les casques donnant des % de récolte de niveau 54 ou plus et de rareté rare")
it1 = Item.findItem(latest["items"], 27717)
it2 = Item.findItem(latest["items"], 2021)
print("CompareTo")
show(it1)
print("CompareFrom")
show(it2)
compare_item = it1.compareFrom(it2)
print("Compare :")
show(compare_item)
it3 = Item.findItemsWithCriterias(latest, [ActionCriteria(31, requestValue=[1]), ActionCriteria(20, requestValue=[53], requestOperator=[">="]), ActionCriteria(150, requestValue=[6], requestOperator=[">="]), ActionCriteria(149, requestValue=[200], requestOperator=[">="])])[0]
print("CompareFrom")
show(it1)
print("CompareTo")
show(it3)
compare_item = it1.compareTo(it3)
print("Compare :")
show(compare_item)
it4 = Recipe.findRecipe(latest['recipes'], 1160)
it5 = Recipe.findRecipeByItemResultName(latest, "Cape Hilare", "fr")[0]
print("CompareFrom")
show(it4)
print("CompareTo")
show(it5)
compare_item = it4.compareTo(it5)
print("Compare :")
show(compare_item)
it6 = Item.findItemsByName(latest['items'], "Epée de Bonta", "fr")[0]
it6.fetchData(latest)
it7 = Item.findItemsByName(latest['items'], "Epée de Brâkmar", "fr")[0]
it7.fetchData(latest)
print("CompareFrom")
show(it6)
print("CompareTo")
show(it7)
compare_item = it6.compareTo(it7)
print("Compare :")
show(compare_item)