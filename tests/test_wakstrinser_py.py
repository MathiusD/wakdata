import unittest
from cdnwakfu import Analyse

class Test_WakStrinser_Py(unittest.TestCase):

    def test_analyse(self):
        string = '[#1] Maîtrise Zone'
        params = [0, 0.8]
        lvl = 50
        result = '40 Maîtrise Zone'
        self.assertEqual(result, Analyse.analyseDescription(string, params, lvl))
        string = '[#1] Point{[>1]?s:} de Vie'
        params = [0, 2, 2, 2]
        lvl = 110
        result = '220 Points de Vie'
        self.assertEqual(result, Analyse.analyseDescription(string, params, lvl))
        string = '{[~3]?[#1] Maîtrise [#3]:[#1] Maîtrise sur [#2] élément{[>2]?s:} aléatoire{[>2]?s:}}'
        params = [160, 0, 3, 0]
        lvl = 200
        result = '160 Maîtrise sur 3 éléments aléatoires'
        self.assertEqual(result, Analyse.analyseDescription(string, params, lvl))
        string = 'Renvoie |[#7.3]*100|% des dégâts'
        params = [0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0.001]
        lvl = 100
        result = 'Renvoie 10% des dégâts'
        self.assertEqual(result, Analyse.analyseDescription(string, params, lvl))
        string = '[#1]% Quantité Récolte{[~2]? en [#2]:}'
        params = [25, 0, 73, 0]
        lvl = 54
        result = '25% Quantité Récolte en 73'
        self.assertEqual(result, Analyse.analyseDescription(string, params, lvl))
        string = '[#1] {[99>3]?:{[0<3]?:{[~3]?([#3]%):}}}'
        params = [25, 0, 73, 0]
        lvl = 54
        result = '25'
        self.assertEqual(result, Analyse.analyseDescription(string, params, lvl))
        string = '[#1] {[99>3]?:{[0<3]?:{[~3]?([#3]%):}}}'
        params = [5885, 0, 1, 0, -1, 0]
        lvl = 200
        result = '5885'
        self.assertEqual(result, Analyse.analyseDescription(string, params, lvl))

        