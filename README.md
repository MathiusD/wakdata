# WakData

![pipeline](https://gitlab.com/MathiusD/wakdata/badges/master/pipeline.svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/25079446a80d4feca753941829a0f0c3)](https://www.codacy.com/manual/MathiusD/wakdata?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/wakdata&amp;utm_campaign=Badge_Grade)

## Concept

Un script très simple permettant de télécharger les dernières versions des jsons contenant les données de jeu, qui se mue en un pseudo ORM pour l'exploitation de ces dernières.

## Utilisation

### Mise en Place

#### Settings

Ce script fonctionne avec un simple fichier de configuration à l'instar du fichier ``config/settings.sample.py``présent dans le dépôt.

```py
# Désigne le service à interroger
URL_API = "https://wakfu.cdn.ankama.com/gamedata/"
# Désigne la ressource contenant le numéro de version
URL_VERSION = "config.json"
# Désigne les fichiers que l'on souhaite récupérer
NAME_JSON = [
    'actions',
    'collectibleResources',
    'equipmentItemTypes',
    'harvestLoots',
    'itemProperties',
    'items',
    'jobsItems',
    'recipeCategories',
    'recipeIngredients',
    'recipeResults',
    'recipes',
    'resourceTypes',
    'resources',
    'states',
    'recipePattern'
]
# Désigne le chemin relatif où l'on souhaite stocker nos données
PATH_DATA = "data"
# Désigne l'emplacement de ce dépôt au sein du dépôt parent
LOCATION_REPO = "."
```

#### Lancement

Pour lancer ce script soit vous créer votre propre `settings.py` que vous placer au sein du répertoire `config`, soit vous utilisez les réglages par défault. Peu importe votre choix, il vous suffira de lancer `sh build.sh` ou `make build` pour finir de configurer le projet.

### Importation

#### Global

Pour importer ou mettre à jour les données. (Vous pouvez également le faire par le biais de ce dépôt via `make fetch`)

```py
from cdnwakfu import import_data

import_data()
```

#### Données Externe

Ce dépôt permet également l'importation de données externes telle que `recipePattern`. Ces données sont donc fanmades et ne sont absolument pas garanties.

##### Exemple

`recipePattern` est une archive json contenant les plans, les recettes qu'il débloquent et leurs lieux d'obtention. Cependant même si l'archive est présente dans le dépôt vous pouvez la mettre à jour à travers ce script. (Attention cela peut prendre pas mal de temps)

```py
from recipes_construction import recipe_construction
from building import recipes_data
from wakdata import latest

#Pour écrire le fichier ainsi produit dans le dossier building
target_folder = "./building"
recipe_construction(latest, recipes_data, target_folder)
```

### Consultation

#### Consultation Globale

Pour consulter les données.

```py
from data import latest
# latest est un dictionnaire contenant les fichiers jsons téléchargés
```

#### Consulation des Recettes

Pour consulter les recettes.

```py
from cdnwakfu import Recipe
from data import latest

recipes = Recipe.findRecipeByItemResultName(latest["recipes"], "La Papoutre", "fr")
# Pour récupérer toutes les recettes dont le nom est composé de "la papoutre" en langue française
Recipe.findRecipe(latest["recipes"], 1160)
# Pour récupérer la recette avec l'id 1160
for recipe in recipes:
    recipe.fetchData(latest)
    # Pour récupérer toutes les données inhérentes à l'objet
    print(recipe.sheet("fr","\t"))
    # Pour afficher la fiche technique de cette recette en langue française avec une tabulation devant chaque ligne
```

#### Consulation des Items et JobItems

Pour consulter les recettes.

```py
from cdnwakfu import Item, JobItem
from data import latest

items = Item.findItemsByName(latest, "La Papoutre", "fr")
# Pour récupérer toutes les items dont le nom est composé de "la papoutre" en langue française
Item.findItem(latest["items"], 2021)
# Pour récupérer l'item avec l'id 2021
for item in items:
    item.fetchData(latest)
    # Pour récupérer toutes les données inhérentes à l'objet
    print(item.sheet("fr","\t"))
    # Pour afficher la fiche technique de cet objet en langue française avec une tabulation devant chaque ligne

jobitems = JobItem.findItemsByName(latest["jobItems"], "Pom", "fr")
# Pour récupérer toutes les jobsitems dont le nom est composé de "pom" en langue française
jobitems.fetchData(lastest["jobItems"])
# Pour récupérer toutes les données inhérentes à l'objet
JobItem.findItem(latest["jobItems"], 1718)
# Pour récupérer l'item avec l'id 2021
for item in jobitems:
    item.fetchData(latest)
    # Pour récupérer toutes les données inhérentes à l'objet
    print(item.sheet("fr","\t"))
    # Pour afficher la fiche technique de cet objet en langue française avec une tabulation devant chaque ligne
```

## Auteur

Féry Mathieu (Aka Mathius)
