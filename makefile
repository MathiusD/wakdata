default_target: build

fetch: rebuild
	@python3 fetch.py

build:
	@sh build.sh

clear:
	@rm -rf own own_install.py data

rebuild: clear build

tests: fetch test

test:
	@rm -f out.txt
	@python3 -m unittest >> out.txt
	@rm tests/*.json

localCraftkfu:
	@python3 craftkfuProduction.py

craftkfu: fetch localCraftkfu