#!/bin/bash

pip install -r requirements_wakdata.txt
SFILE=./config_wakdata/settings.py
if ! [ -f "$SFILE" ]; then
    cp ./config_wakdata/settings.sample.py $SFILE
fi
FILE=../own_install.py
LFILE=./own_install.py
if ! [ -f "$LFILE" ]; then
    if [ -f "$FILE" ]; then
        cp $FILE $LFILE
    else
        git clone https://gitlab.com/MathiusD/owninstall
        cp owninstall/install.py own_install.py
        rm -rf owninstall
    fi
fi
python3 build.py