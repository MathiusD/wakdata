from ..Texts import Text

class State:
    """
    Classe représentant un état au sein
    des donneés d'Ankama.
    """

    def __init__(self, StateData:dict):
        self.id = StateData["definition"]["id"]
        if "title" in StateData:
            try:
                self.title = Text(StateData["title"])
            except AttributeError:
                self.title = Text({
                    "fr":"Etat Inconnu" if StateData["title"]["fr"] in [None, ""] else StateData["title"]["fr"],
                    "en":"Unknow State"
                })
        else:
            self.title = None
        if "description" in StateData:
            try:
                self.description = Text(StateData["description"])
            except AttributeError:
                self.description = Text({
                    "fr":"Description Inconnue" if StateData["description"]["fr"] in [None, ""] else StateData["description"]["fr"],
                    "en":"Unknow Description"
                })
        else:
            self.description = None

    def __str__(self):
        return self.title.__str__()

    @staticmethod
    def findState(states:list, id:int):
        """
        Méthode permettant de rechercher une action avec
        l'id ciblée au sein des données d'Ankama.
        """
        for state in states:
            if id == state["definition"]["id"]:
                return State(state)
        return None