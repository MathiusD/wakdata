from .criteria import Criteria
from ..Utils import Analyse, ComparatorStr
from ..Effects import Effect

class ActionCriteria(Criteria):
    """
    Classe représentant un critère
    de recherche selon une action
    donnée.
    """

    def __init__(self, action:int, argLen:int = None, requestValue:list = None, requestOperator:list = None):
        self.action = action
        self.argLen = argLen
        self.requestValue = requestValue
        self.requestOperator = requestOperator

    def isMatched(self, effect:Effect, level:int):
        result = False
        if effect.actionId == self.action:
            result = True
            result = False if self.argLen and len(effect.params)/2 != self.argLen else result
            if result and self.requestValue:
                operators = []
                if self.requestOperator:
                    for operate in self.requestOperator:
                        operat = ComparatorStr(operate) if operate else ComparatorStr()
                        operators.append(operat)
                else:
                    for indice in range(len(self.requestValue)):
                        operators.append(ComparatorStr())
                for indice in range(len(self.requestValue)):
                    result = False if result and self.requestValue[indice] and not operators[indice].process(Analyse._calc_value(indice + 1, effect.params, level), self.requestValue[indice]) else result
        return result