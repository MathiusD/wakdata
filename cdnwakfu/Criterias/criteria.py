import abc

class Criteria:
    """
    Classe représentant un critère
    de recherche.
    """

    @abc.abstractmethod
    def isMatched(self, target):
        """
        Méthode permettant de déterminer si
        le critère correspond avec la cible.
        """
        pass