from .criteria import Criteria
from ..EquipementItemTypes import EquipementItemType

class EquipementItemTypeCriteria(Criteria):
    """
    Classe représentant un critère de
    recherche selon un type d'équipement
    donné.
    """

    def __init__(self, idSearch:int):
        self.idSearch = idSearch

    def isMatched(self, equipementItemType:EquipementItemType):
        return True if equipementItemType.definition.id == self.idSearch else False