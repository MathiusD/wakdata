from .criteria import Criteria
from ..Utils import ComparatorStr

class LevelCriteria(Criteria):
    """
    Classe représentant un critère de
    recherche selon un niveau donné.
    """

    def __init__(self, level:int, requestOperator:str = None):
        self.level = level
        self.requestOperator = ComparatorStr(requestOperator)

    def isMatched(self, level:int):
        return True if self.requestOperator.process(self.level, level) else False