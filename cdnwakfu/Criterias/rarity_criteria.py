from .criteria import Criteria
from ..Utils import ComparatorStr
from ..Raritys import Rarity

class RarityCriteria(Criteria):
    """
    Classe représentant un critère de
    recherche selon une rareté donnée.
    """

    def __init__(self, rarity:Rarity):
        self.rarity = rarity.value

    def isMatched(self, rarity:int):
        return True if rarity == self.rarity else False