from ..ContainItems import ContainItem
from ..Utils import Dichot

class RecipeIngredient(ContainItem):
    """
    Classe représentant l'ingrédient d'une recette
    au sein des données d'Ankama.
    """

    def __init__(self, RecipeIngredientsData:dict):
        self.recipeId = RecipeIngredientsData["recipeId"]
        self.itemId = RecipeIngredientsData["itemId"]
        self.quantity = RecipeIngredientsData["quantity"]
        self.ingredientOrder = RecipeIngredientsData["ingredientOrder"]
        self.item = None
        super().__init__(self.itemId, self.quantity, RecipeIngredientsData["id"] if "id" in RecipeIngredientsData else None)

    @classmethod
    async def asyncFindRecipeIngredients(cls, recipeIngredients:list, id:int):
        """
        Méthode asynchrone permettant de rechercher l'ingrédient
        d'une recette avec l'id donnée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeIngredients, (recipeIngredients, id))

    @staticmethod
    def findRecipeIngredients(recipeIngredients:dict, id:int):
        """
        Méthode permettant de rechercher l'ingrédient d'une recette
        avec l'id donnée au sein des données d'Ankama.
        """
        ingredients = []
        for ingredient in recipeIngredients:
            if ingredient['recipeId'] == id:
                ingredients.append(RecipeIngredient(ingredient))
        return ingredients

    @classmethod
    async def asyncFindRecipeIngredientsWithRecipe(cls, recipeIngredients:list, recipe):
        """
        Méthode asynchrone permettant de rechercher l'ingrédient
        d'une recette donnée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeIngredientsWithRecipe, (recipeIngredients, recipe))

    @classmethod
    def findRecipeIngredientsWithRecipe(cls, recipeIngredients:dict, recipe):
        """
        Méthode permettant de rechercher l'ingrédient d'une recette
        avec la recette donnée au sein des données d'Ankama.
        """
        if recipe.ingredients:
            ingredients = []
            for ingredient in recipe.ingredients:
                search = Dichot(recipeIngredients, ingredient, ["id"])
                if search:
                    ingredients.append(RecipeIngredient(search))
            return ingredients
        else:
            return cls.findRecipeIngredients(recipeIngredients, recipe.id)