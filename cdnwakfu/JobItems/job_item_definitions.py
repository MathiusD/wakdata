from ..Parameters import GraphicParameters

class JobItemDefinition:
    """
    Classe comportant des données liées
    à un JobItem dans les données d'Ankama.
    """

    def __init__(self, JobItemDefinitionData:dict):
        self.id = JobItemDefinitionData["id"]
        self.level = JobItemDefinitionData["level"]
        self.rarity = JobItemDefinitionData["rarity"]
        self.graphicParameters = GraphicParameters(JobItemDefinitionData["graphicParameters"])