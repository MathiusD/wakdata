from ..Utils import Dichot
from ..Texts import Text
from ..JobItems import JobItemDefinition
from ..Raritys import Rarity
from ..BaseItems import BaseItem

class JobItem(BaseItem):
    """
    Classe représentant un objet lié uniquement
    à l'artisanat au sein des données d'Ankama.
    """

    def __init__(self, JobItemData:dict):
        self.definition = JobItemDefinition(JobItemData["definition"])
        try:
            title = Text(JobItemData["title"]) if "title" in JobItemData else None
        except AttributeError:
            title = Text({
                "fr":"Item Inconnu" if JobItemData["title"]["fr"] in [None, ""] else JobItemData["title"]["fr"],
                "en":"Unknow Item"
            })
        super().__init__(self.definition.id, title, self.definition.graphicParameters, JobItemData["recipes"] if "recipes" in JobItemData else None)

    @property
    def encyclopediaLink(self):
        provider = "https://www.wakfu.com/"
        return Text({
            "fr":"%sfr/mmorpg/encyclopedie/ressources/%i" % (provider, self._id),
            "en":"%sen/mmorpg/encyclopedia/resources/%i" % (provider, self._id),
            "es":"%ses/mmorpg/enciclopedia/recursos/%i" % (provider, self._id),
            "pt":"%spt/mmorpg/enciclopedia/recursos/%i" % (provider, self._id)
        })

    def __str__(self):
        return self.title.__str__() if self.title else super().__str__()

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 5):
        super()._row_sheet(lang, prefix, recursive)
        jonclvl = Text({"fr":"Niveau","en":"Level","es":"Nivel","pt":"Nível"})
        self._row.append("%s : %i" % (jonclvl.getByShortLang(self._lang) ,self.definition.level))
        if self.definition:
            rarity = Text({"fr":"Rareté", "en":"Rarity", "es":"Rareza", "pt":"Raridade"})
            msg = "%s : %s" % (rarity.getByShortLang(lang), Rarity.RaritytoText(self.definition.rarity).getByShortLang(lang))
            self._row.append(msg)
        return {"data":self._row,"prefix":prefix}

    @classmethod
    async def asyncFindItemsByName(cls, jobItems:list, name:str, lang:str):
        """
        Méthode asynchrone permettant de rechercher tout les JobItems
        correspondant au nom dans la langue donnée.
        """
        return await cls.asyncSearch(cls.findItemsByName, (jobItems, name, lang))

    @classmethod
    def findItemsByName(cls, jobItems:list, name:str, lang:str):
        """
        Méthode permettant de rechercher tout les JobItems
        correspondant au nom dans la langue donnée.
        """
        items = []
        threads = []
        name = name.strip().lower()
        for item in jobItems:
            ite = JobItem(item)
            cls._defineThread(cls.processForName, (ite, name, lang, items), threads)
        cls._waitingThreads(threads)
        return items

    @staticmethod
    def processForName(item, name:str, lang:str, items:list):
        if item.title:
            if name in (item.title.getByShortLang(lang).lower().strip()):
                items.append(item)

    @classmethod
    async def asyncFindItem(cls, jobItems:list, id:int):
        """
        Méthode asynchrone permettant de rechercher
        tout les JobItems avec l'id donné.
        """
        return await cls.asyncSearch(cls.findItem, (jobItems, id))

    @staticmethod
    def findItem(jobItems:list, id:int):
        """
        Méthode permettant de rechercher
        tout les JobItems avec l'id donné.
        """
        search = Dichot(jobItems, id, ['definition', 'id'])
        return JobItem(search) if search else None