from ..Elements import UserReadableElement, CompareElement, SearchElement
from ..Texts import Text
from ..HarvestLoots import HarvestLoot
from ..Parameters import GraphicParameters
import abc

class BaseItem(UserReadableElement, CompareElement, SearchElement):
    """
    Classe représentant les données de base d'un
    Objet au sein des données d'Ankama.
    """

    def __init__(self, id:int, title:Text, graphicParameters:GraphicParameters, recipes:list = None):
        super().__init__()
        self._id = id
        self.loots = []
        self.title = title
        self.recipes = recipes
        self._graphicParameters = graphicParameters

    @property
    def craftkfuLink(self):
        return "https://craftkfu.waklab.fr/?%i" % (self._id)

    @property
    @abc.abstractmethod
    def encyclopediaLink(self):
        pass

    def hasCraft(self):
        if self.recipes:
            return True if len(self.recipes) > 0 else False
        return False

    def fetchData(self, GameData:dict, recursive:int = 4):
        super()._recursion(recursive)
        if "harvestLoots" in GameData.keys() and len(self.loots) == 0:
            self.loots = HarvestLoot.findHarvestLoots(GameData["harvestLoots"], self._id)
        if self._recursive:
            for listloot in self.loots:
                for loot in listloot:
                    loot.fetchData(GameData, self._recursive)

    def compareTo(self, item, data=None):
        compare = super().compareTo(item)
        if compare:
            compare = self._compareListAttr(compare, "loots", item)
            compare.title = None
            return compare
        return None
    
    def _reverted(self):
        reverted = super()._reverted()
        for loot in reverted.loots:
            loot._reverted()
        return reverted

    def __str__(self):
        return str(self._id)

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 5):
        super()._row_sheet(lang, prefix, recursive)
        if self.title:
            title = Text({"fr":"Nom", "en":"Name", "es":"Nombre","pt":"Nome"})
            self._row.append("%s : %s" % (title.getByShortLang(lang), self.title.getByShortLang(self._lang)))
        if self.encyclopediaLink:
            link = Text({"fr":"Lien", "en":"Link", "es":"Enlace","pt":"Link"})
            self._row.append("%s : %s" % (link.getByShortLang(lang), self.encyclopediaLink.getByShortLang(self._lang)))
        self._row.append("Craftkfu : %s" % (self.craftkfuLink))
        if self.loots:
            if len(self.loots) > 0:
                jonc = Text({"fr":"Obtention au sein de ","en":"Getting within","es":"Entrando en el interior","pt":"Obtenção dentro de"})
                self._row.append("\t%s:" % jonc.getByShortLang(lang))
                for lootlist in self.loots:
                    msg = {}
                    for loot in lootlist:
                        if loot.collectibleResources:
                            for collectibleResource in loot.collectibleResources:
                                Index = collectibleResource.resourceIndex
                                if not(Index in msg.keys()):
                                    if collectibleResource.resource:
                                        msg[Index] = {}
                                        state = Text({"fr":"Etat","en":"State","es":"Estado","pt":"Estado"})
                                        msg[Index]["msg"] = "\t\tIn %s [%s : %s]" % (collectibleResource.resource.title.getByShortLang(self._lang), state.getByShortLang(lang), collectibleResource.resourceIndex)
                                        if collectibleResource.resource.definition:
                                            if collectibleResource.resource.definition.resourceType:
                                                typ = Text({"fr":"Type","en":"Type","es":"Escriba","pt":"Datilografar"})
                                                msg[Index]["msg"] = "%s (%s : %s)" % (msg[Index]["msg"], typ.getByShortLang(lang), collectibleResource.resource.definition.resourceType.title.getByShortLang(self._lang))
                                    msg[Index]["rate"] = []
                                msg[Index]["rate"].append(loot.quantityPerItem * loot.dropRate)
                    out = {}
                    for msgi in msg:
                        out = 0
                        for rate in msg[msgi]["rate"]:
                            out += rate
                        average = Text({"fr":"Drop Moyen","en":"Average Drop","es":"Caída media","pt":"Baixa média"})
                        self._row.append("%s (%s : %f)" % (msg[msgi]["msg"], average.getByShortLang(lang),out/len(msg[msgi]["rate"])))
        return {"data":self._row,"prefix":prefix}

