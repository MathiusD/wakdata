class ActionDefinition:
    """
    Classe représentant des données nécessaire
    aux actions au sein des données d'Ankama.
    """

    def __init__(self, ActionDefinitionData:dict):
        self.id = ActionDefinitionData["id"]
        self.effect = ActionDefinitionData["effect"]

    def __str__(self):
        return self.effect