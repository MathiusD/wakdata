from ..Utils import Analyse, Dichot
from ..Actions import ActionDefinition
from ..Texts import Text
from ..Elements import SearchElement

class Action(SearchElement):
    """
    Classe représentant une action au
    sein des données d'Ankama.
    """

    def __init__(self, ActionData:dict):
        self.definition = ActionDefinition(ActionData["definition"])
        if "description" in ActionData:
            try:
                self.description = Text(ActionData["description"])
            except AttributeError:
                self.description = Text({
                    "fr":"Action Inconnue" if ActionData["description"]["fr"] in [None, ""] else ActionData["description"]["fr"],
                    "en":"Unknow Action"
                })
        else:
            self.description = None

    def adaptingDescription(self, paramsEffect:list, lvl:int):
        """
        Méthode permettant d'obtenir la description de l'action
        adapté aux paramètres enregistrés.
        """
        if self.description:
            out = {}
            for lang in self.description.langsOfText:
                out[lang.nameAttr] = Analyse.analyseDescription(self.description.getByLang(lang), paramsEffect, lvl)
                out[lang.nameAttr] = Analyse.defineElements(out[lang.nameAttr], lang.nameAttr)
            return Text(out)
        return self.definition.effect

    @staticmethod
    def findAction(actions:list, id:int):
        """
        Méthode permettant de trouver une action avec l'id
        spécifié au sein des données d'Ankama.
        """
        search = Dichot(actions, id, ['definition', 'id'])
        return Action(search) if search else None

    @classmethod
    async def asyncFindAction(cls, actions:list, id:int):
        """
        Méthode asynchrone permettant de trouver une action
        avec l'id spécifié au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findAction, (actions, id))

    def __str__(self):
        return self.description.__str__() if self.description else self.definition.__str__()