from ..States import State
from ..Actions import Action
from ..Texts import Text
from ..Elements import UserReadableElement, CompareElement
from ..RecipeCategories import RecipeCategorie
from ..Utils import Analyse

class Effect(UserReadableElement, CompareElement):
    """
    Classe représentant un effet au sein des données d'Ankama.
    """
    
    def __init__(self, EffectData:dict, subEffects:dict = None):
        super().__init__()
        self.id = EffectData["definition"]["id"]
        self.actionId = EffectData["definition"]["actionId"]
        self.areaShape = EffectData["definition"]["areaShape"]
        self.areaSize = EffectData["definition"]["areaSize"]
        self.params = EffectData["definition"]["params"]
        try:
            self.description = Text(EffectData["description"]) if "description" in EffectData.keys() else None
        except AttributeError:
            self.description = Text({
                "fr":"Effet Inconnu" if EffectData["description"]["fr"] in [None, ""] else EffectData["description"]["fr"],
                "en":"Unknow Effect"
            })
        self.action = None
        self.effects = None
        self.subEffects = None
        if subEffects:
            self.subEffects = []
            for subEffect in subEffects:
                self.subEffects.append(Effect(subEffect["effect"]))

    def fetchData(self, GameData:dict, lvl:int, recursive:int = 3):
        super()._recursion(recursive)
        if "actions" in GameData.keys() and not self.action:
            self.action = Action.findAction(GameData["actions"], self.actionId)
        if self.action:
            self.effects = []
            op = True
            data = self.action.adaptingDescription(self.params, lvl)
            if "states" in GameData.keys():
                if self._recursive:
                    if self.action.definition.effect.startswith("State"):
                        stateId = int(data.fr.split(" ")[0])
                        state = State.findState(GameData["states"], stateId)
                        if state:
                            if state.title:
                                for lang in data.langsOfText:
                                    setattr(data, lang.nameAttr, data.getByLang(lang).replace(str(stateId), state.title.getByLang(lang)))
                                if state.description:
                                    for lang in data.langsOfText:
                                        setattr(data, lang.nameAttr, "%s[%s]" % (data.getByLang(lang), state.description.getByLang(lang)))
                            else:
                                op = False
            if "recipeCategories" in GameData.keys():
                if self._recursive:
                    if self.action.definition.effect.startswith("Job"):
                        if data.fr.count("en") > 0:
                            CatId = int(data.fr.split("en")[1])
                            cat = RecipeCategorie.findRecipeCategorie(GameData["recipeCategories"], CatId)
                        if cat:
                            for lang in data.langsOfText:
                                setattr(data, lang.nameAttr, data.getByLang(lang).replace(str(CatId), cat.title.getByLang(lang)))
            if self._recursive:
                if self.action.definition.effect.startswith("Gain : charac passée en paramètre") and self.description is not None:
                    CaracId = int(data.fr.split(" ")[1])
                    for lang in data.langsOfText:
                        if data.fr.count(":") == 1:
                            setattr(data, lang.nameAttr, data.getByLang(lang).replace(str(CaracId), Analyse.analyseDescription(self.description.getByLang(lang), self.params, lvl)))
                        else:
                            setattr(data, lang.nameAttr, Analyse.analyseDescription(self.description.getByLang(lang), self.params, lvl))
            if not self.action.definition.effect.startswith("NullEffect") and op == True:
                self.effects.append(data)
        if self._recursive and self.subEffects:
            for subEffect in self.subEffects:
                subEffect.fetchData(GameData, lvl, self._recursive)

    def compareTo(self, effect, lvls:list):
        compare = super().compareTo(effect)
        if self.actionId == effect.actionId and compare and len(self.params) == len(effect.params) and not self.action.definition.effect.startswith("State") and not self.action.definition.effect.startswith("Job") and not self.action.definition.effect.startswith("Gain : charac passée en paramètre") and not self.action.definition.effect.startswith("NullEffect") and not self.action.definition.effect.startswith("REG"):
            compare.params = []
            for indice in range(round(len(self.params)/2) + 1):
                val1 = Analyse._calc_value(indice + 1, self.params, lvls[0])
                val2 = Analyse._calc_value(indice + 1, effect.params, lvls[1])
                value = val2 - val1
                compare.params.append(value)
                compare.params.append(0)
            compare.description = None
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        if not self.action.definition.effect.startswith("NullEffect"):
            if self.action.definition.effect.startswith("State"):
                reverted.params = "-%s" % reverted.params
            else:
                if self.action.definition.effect.startswith("REG"):
                    for subEffect in self.subEffects:
                        subEffect._reverted()
                else:
                    for indice in range(round(len(self.params)/2) + 1):
                        exploited_index = ((indice - 1) * 2)
                        reverted.params[exploited_index] = self.params[exploited_index] * -1
        return reverted

    def __str__(self):
        return str(self.id)

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 3):
        super()._row_sheet(lang, prefix, recursive)
        if self.effects:
            for effect in self.effects:
                if isinstance(effect, Text):
                    self._row.append(effect.getByShortLang(self._lang))
                else:
                    if effect.startswith("REG") and self._recursive:
                        if self.subEffects:
                            for subEffect in self.subEffects:
                                self._row.append(subEffect._row_sheet(self._lang, "\t", self._recursive))
                        else:
                            self._row.append(effect)
                    else:
                        self._row.append(effect)
        return {"data":self._row,"prefix":prefix}