from cdnwakfu.Raritys import Rarity
xelorium = {
    "fr": "Obtention à Xélorium et chez Kali (Archi et Quêtes Environnementales)",
    "en": "Droped at Xelorium and Kali (Archi and Environmental Quests)",
    "es": "Dejado caer en Xelorium y Kali (Archi y Environmental Quests)",
    "pt": "Deixado em Xelorium e Kali (Archi e Environmental Quests)"
}
moon = {
    "fr": "Obtention à Moon (Archi et Quêtes Environnementales)",
    "en": "Droped in Moon (Archi and Environmental Quests)",
    "es": "Pasado luna (Archi y Environmental Quests)",
    "pt": "Vindo buscar lua (Archi e Environmental Quests)"
}
zinit = {
    "fr": "Obtention à Zinit (Archi et Quêtes Environnementales)",
    "en": "Droped in Zinit (Archi and Environmental Quests)",
    "es": "Pasado Zinit (Archi y Environmental Quests)",
    "pt": "Vindo buscar Zinit (Archi e Environmental Quests)"
}
flaqueux = {
    "fr": "Quêtes Environnementales des Flaqueux et des Pichons",
    "en": "Puddly and Snappers Environmental Quests",
    "es": "Puddly y Snappers Environmental Quests",
    "pt": "Puddly e Snappers Environmental Quests"
}
blop = {
    "fr": "Quêtes Environnementales des Blops",
    "en": "Blops Environmental Quests",
    "es": "Blops búsquedas ambientales",
    "pt": "Blops indagações ambientais"
}
abraknyde = {
    "fr": "Quêtes Environnementales des Abraknydes Sombres",
    "en": "Dark Threechnids Environmental Quests",
    "es": "Búsquedas ambientales Threechnids oscuras",
    "pt": "Indagações ambientais Threechnids escuras"
}
riktus = {
    "fr": "Quêtes Environnementales des Magik Riktus",
    "en": "Magik Riktus Environmental Quests",
    "es": "Magik Riktus búsquedas ambientales",
    "pt": "Magik Riktus indagações ambientais"
}
trefonds = {
    "fr": "Quêtes Environnementales des Tréfonds des Mines de Nations",
    "en": "Environmental Quests of the Nation's Mines Bottomlands",
    "es": "Búsquedas ambientales de las minas nacionales Bottomlands",
    "pt": "Indagações ambientais das minas nacionais Bottomlands"
}
heros = {
    "fr": "Héros des écosystèmes de",
    "en": "Ecosystems Heros of",
    "es": "Ecosistemas Heros de",
    "pt": "Ecossistemas Heros de"
}
mimic = "Mimic"
mercenaires = {
    "fr": "Exploit des Mercenaires",
    "en": "Mercenaries Achievements",
    "es": "Logros de mercenarios",
    "pt": "Realizações de mercenários"
}
egouts = {
    "fr": "Avoir terminé les Egouts qui ont du goût (Quêtes Principale)",
    "en": "Complete the Tasteful Sewers (Main Quest)",
    "es": "Complete las alcantarillas de buen gusto (búsqueda principal)",
    "pt": "Conclua os tubos saborosos (indagação principal)"
}
klime = {
    "fr": "Quêtes de Klime",
    "en": "Klime's Quests",
    "es": "Las búsquedas de Klime",
    "pt": "As indagações de Klime"
}
sberg = {
    "fr": "Exploit des Livres de Sberg",
    "en": "Sbergs books Achievements",
    "es": "Sbergs reserva Logros",
    "pt": "Sbergs reserva Realizações"
}
Otomai = {
    "fr": "Quêtes d'Otomaï",
    "en": "Otomai's Quests",
    "es": "Las búsquedas de Otomai",
    "pt": "As indagações de Otomai"
}
ruel = {
    "fr": "Quête de Ruel",
    "en": "Ruel's Quests",
    "es": "Las búsquedas de Ruel",
    "pt": "As indagações de Ruel"
}
fool = {
    "fr": "Quêtes de Foul Moon",
    "en": "Sham Moon's Quests",
    "es": "Las búsquedas de la luna del impostor",
    "pt": "As indagações de lua de impostura"
}
ombrage = {
    "fr": "Obtention Ombrage",
    "en": "Droped on Ombrage",
    "es": "Dejado caer en Ombrage",
    "pt": "Deixado em Ombrage"
}
madra = {
    "fr": "Quêtes de la Mâdrâgue",
    "en": "Madrague's Quests",
    "es": "Las búsquedas de Madrague",
    "pt": "As indagações de Madrague"
}
nowel = {
    "fr": "Quêtes de Nowel",
    "en": "Nowel Quests",
    "es": "Búsquedas de Nowel",
    "pt": "Indagações de Nowel"
}
chuchoku = {
    "fr": "Quêtes de Chuchoku",
    "en": "Shhhudoku Quests",
    "es": "Búsquedas de Susurrón",
    "pt": "Indagações de Shiudoku"
}
gouv = {
    "fr": "Exploit d'avoir été gouverneur",
    "en": "Exploitation of having been governor",
    "es": "Explotación de haber sido gobernador",
    "pt": "Exploração de ter sido governador"
}
mercenaryOldAmakna = {
    "fr": "%s de l'ancienne Amakna (Plus obtentible)" % (mercenaires['fr']),
    "en": "%s of the old Amakna (No longer obtainable)" % (mercenaires['en']),
    "es": "%s de la antigua Amakna (ya no se puede obtener)" % (mercenaires['es']),
    "pt": "%s da velho Amakna (já não é possível obtê-la)" % (mercenaires['pt'])
}
mercenaryOldBonta = {
    "fr": "%s de l'ancienne Bonta (Plus obtentible)" % (mercenaires['fr']),
    "en": "%s of the old Bonta (No longer obtainable)" % (mercenaires['en']),
    "es": "%s de la antigua Bonta (ya no se puede obtener)" % (mercenaires['es']),
    "pt": "%s da velho Bonta (já não é possível obtê-la)" % (mercenaires['pt'])
}
mercenaryOldBrakmar = {
    "fr": "%s de l'ancienne Brâkmar (Plus obtentible)" % (mercenaires['fr']),
    "en": "%s of the old Brâkmar (No longer obtainable)" % (mercenaires['en']),
    "es": "%s de la antigua Brâkmar (ya no se puede obtener)" % (mercenaires['es']),
    "pt": "%s da velho Brâkmar (já não é possível obtê-la)" % (mercenaires['pt'])
}
mercenaryOldSufokia = {
    "fr": "%s de l'ancienne Sufokia (Plus obtentible)" % (mercenaires['fr']),
    "en": "%s of the old Sufokia (No longer obtainable)" % (mercenaires['en']),
    "es": "%s de la antigua Sufokia (ya no se puede obtener)" % (mercenaires['es']),
    "pt": "%s da velho Sufokia (já não é possível obtê-la)" % (mercenaires['pt'])
}
myth_leg = [
    Rarity.Mythique.value,
    Rarity.Légendaire.value
]
rar_myth_leg = [
    Rarity.Rare.value,
    Rarity.Mythique.value,
    Rarity.Légendaire.value
]
epiq = [
    Rarity.Epique.value
]
reliq = [
    Rarity.Relique.value
]
souv = [
    Rarity.Souvenir.value
]
myth = [
    Rarity.Mythique.value
]
rare = [
    Rarity.Rare.value
]
leg = [
    Rarity.Légendaire.value
]
rar_myth = [
    Rarity.Rare.value,
    Rarity.Mythique.value
]
recipes_data = [
    {
        "name": "Armure du Disciple d'Otomaï",
        "obtention":
        {
            "fr": "%s V" % Otomai["fr"],
            "en":"%s V" % Otomai["en"],
            "es":"%s V" % Otomai["es"],
            "pt":"%s V" % Otomai["pt"]
        },
        "item":reliq
    },
    {
        "name": "Memento Mori",
        "obtention": {
            "fr": "%s V" % ruel["fr"],
            "en":"%s V" % ruel["en"],
            "es":"%s V" % ruel["es"],
            "pt":"%s V" % ruel["pt"]
        },
        "item":reliq
    },
    {
        "name": "Plastron Kroa",
        "obtention": {
            "fr": "%s VI" % ruel["fr"],
            "en":"%s VI" % ruel["en"],
            "es":"%s VI" % ruel["es"],
            "pt":"%s VI" % ruel["pt"]
        },
        "item":leg
    },
    {
        "name": "Zespaulettes en Ivoire",
        "obtention": flaqueux,
        "item": myth_leg
    },
    {
        "name": "Bouclivore",
        "obtention": blop,
        "item": rar_myth_leg
    },
    {
        "name": "Gloutopaulettes",
        "obtention": blop,
        "item": myth_leg
    },
    {
        "name": "Gloutotron",
        "obtention": blop,
        "item": rar_myth_leg
    },
    {
        "name": "Epaulettes Paradoxales",
        "obtention": xelorium,
        "item": myth_leg
    },
    {
        "name": "Cotte Côt",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Les Epaupeyes",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Peignoir Dodikaprio",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Piquants Solaires",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Plastron Dunîl",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Rempart Siramparla",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Epaulettes du Roi Singe",
        "obtention": fool,
        "item": reliq
    },
    {
        "name": "L'Epoilé",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Nargilet",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Crocs Féroces",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Anneau Fréquent",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Jonc Tueux",
        "obtention": xelorium,
        "item": myth_leg
    },
    {
        "name": "Cire-Anneau",
        "obtention": {
            "fr": "%s IV" % ruel["fr"],
            "en":"%s IV" % ruel["en"],
            "es":"%s IV" % ruel["es"],
            "pt":"%s IV" % ruel["pt"]
        },
        "item":myth
    },
    {
        "name": "Anneau Abraknyde Impérial",
        "obtention": abraknyde,
        "item": myth_leg
    },
    {
        "name": "Le Nomade Max",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Anneau Pet",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Siroccollier",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Amulette Abraknyde Impérial",
        "obtention": abraknyde,
        "item": myth_leg
    },
    {
        "name": "L'Anneau Rang-Outen",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Gloutollier",
        "obtention": blop,
        "item": myth_leg
    },
    {
        "name": "Bracelet Triqué",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Oeuf Shamanique",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Grigri d'Romel",
        "obtention": zinit,
        "item": rar_myth_leg
    },
    {
        "name": "Gri-gri Primitif",
        "obtention": xelorium,
        "item": rar_myth_leg
    },
    {
        "name": "Cire-Anneau",
        "obtention": {
            "fr": "%s IX" % ruel["fr"],
            "en":"%s IX" % ruel["en"],
            "es":"%s IX" % ruel["es"],
            "pt":"%s IX" % ruel["pt"]
        },
        "specific_name":"Plan \"Cire-Anneau\" (Souvenir)",
        "item":souv
    },
    {
        "name": "La Broche Céleste",
        "obtention": trefonds,
        "item": myth_leg
    },
    {
        "name": "Sinistrano",
        "obtention": trefonds,
        "item": myth_leg
    },
    {
        "name": "Anneau de Brâkmar",
        "obtention": {
            "fr": "%s de Brâkmar" % heros["fr"],
            "en":"%s of Brâkmar" % heros["en"],
            "es":"%s de Brâkmar" % heros["es"],
            "pt":"%s de Brâkmar" % heros["pt"]
        },
        "item":epiq
    },
    {
        "name": "Anneau de Bonta",
        "obtention": {
            "fr": "%s de Bonta" % heros["fr"],
            "en":"%s of Bonta" % heros["en"],
            "es":"%s de Bonta" % heros["es"],
            "pt":"%s de Bonta" % heros["pt"]
        },
        "item":epiq
    },
    {
        "name": "Anneau de Sufokia",
        "obtention": {
            "fr": "%s de Sufokia" % heros["fr"],
            "en":"%s of Sufokia" % heros["en"],
            "es":"%s de Sufokia" % heros["es"],
            "pt":"%s de Sufokia" % heros["pt"]
        },
        "item":epiq
    },
    {
        "name": "Anneau d'Amakna",
        "obtention": {
            "fr": "%s d'Amakna" % heros["fr"],
            "en":"%s of Amakna" % heros["en"],
            "es":"%s de Amakna" % heros["es"],
            "pt":"%s de Amakna" % heros["pt"]
        },
        "item":epiq
    },
    {
        "name": "Petits Sâblés",
        "obtention": madra,
        "specific_name": "Recette des \"Petits Sâblés\""
    },
    {
        "name": "La Pâtée de Chacha",
        "obtention": {
            "fr": "%s de la Cité d'Astrub" % mercenaires["fr"],
            "en":"%s of Astrub City" % mercenaires["en"],
            "es":"%s de la ciudad de Astrub" % mercenaires["es"],
            "pt":"%s de Astrub City" % mercenaires["pt"]
        },
        "skip":True
    },
    {
        "name": "Piouchée à la Reine",
        "obtention": {
            "fr": "%s des Alentours d'Astrub" % mercenaires["fr"],
            "en":"%s Surroundings of Astrub" % mercenaires["en"],
            "es":"%s Alrededores de Astrub" % mercenaires["es"],
            "pt":"%s Meio de Astrub" % mercenaires["pt"]
        },
        "skip":True
    },
    {
        "name": "Pizz'larve du Fhenrat",
        "obtention": {
            "fr": "%s des égouts d'Astrub" % mercenaires["fr"],
            "en":"%s sewers of Astrub" % mercenaires["en"],
            "es":"%s alcantarillas de Astrub" % mercenaires["es"],
            "pt":"%s tubos de Astrub" % mercenaires["pt"]
        },
        "skip":True
    },
    {
        "name": "Fricassée de Corbec à la Kroapule",
        "obtention": {
            "fr": "%s de Kelba" % mercenaires["fr"],
            "en":"%s of Kelba" % mercenaires["en"],
            "es":"%s de Kelba" % mercenaires["es"],
            "pt":"%s de Kelba" % mercenaires["pt"]
        },
        "skip":True
    },
    {
        "name": "Egreu-Visse à l'Escamèche",
        "obtention": moon,
    },
    {
        "name": "Pex on the beach",
        "obtention": moon
    },
    {
        "name": "Pizz'larve",
        "obtention": egouts,
        "specific_name": "Recette \"Pizz'larve\""
    },
    {
        "name": "Emblème du chasseur d'Archimonstres",
        "obtention": {
            "fr": "%s VII" % ruel["fr"],
            "en":"%s VII" % ruel["en"],
            "es":"%s VII" % ruel["es"],
            "pt":"%s VII" % ruel["pt"]
        },
        "item":leg
    },
    {
        "name": "La Queue",
        "obtention": {
            "fr": "%s I" % ruel["fr"],
            "en":"%s I" % ruel["en"],
            "es":"%s I" % ruel["es"],
            "pt":"%s I" % ruel["pt"]
        },
        "item":leg
    },
    {
        "name": "Gourdin Cornu",
        "obtention": riktus,
        "item": myth
    },
    {
        "name": "Gourdin des Rois",
        "obtention": riktus,
        "item": myth
    },
    {
        "name": "La Défonce",
        "obtention": riktus,
        "item": rar_myth
    },
    {
        "name": "Rhizome Dorée",
        "obtention": riktus,
        "item": rar_myth
    },
    {
        "name": "Abraknydi Vivitus",
        "obtention": abraknyde,
        "item": rar_myth_leg
    },
    {
        "name": "Dague Intemporelle",
        "obtention": xelorium,
        "item": myth_leg
    },
    {
        "name": "Aiguille de Minuit",
        "obtention": trefonds,
        "item": rar_myth_leg
    },
    {
        "name": "Arc Riktus Elite",
        "obtention": trefonds,
        "item": rar_myth_leg
    },
    {
        "name": "Epée brisée",
        "obtention": trefonds,
        "item": myth_leg
    },
    {
        "name": "Epée Riktus Elite",
        "obtention": trefonds,
        "item": rar_myth_leg
    },
    {
        "name": "La Papoutre",
        "obtention": trefonds,
        "item": myth_leg
    },
    {
        "name": "Le Seum",
        "obtention": trefonds,
        "item": myth_leg
    },
    {
        "name": "Stalagmite Géante",
        "obtention": trefonds,
        "item": rar_myth_leg
    },
    {
        "name": "Cartes à Crocs",
        "obtention": moon,
        "item": leg
    },
    {
        "name": "L'Aighuileuse",
        "obtention": moon,
        "item": leg
    },
    {
        "name": "La Recssenette",
        "obtention": moon,
        "item": leg
    },
    {
        "name": "Marteau Luterking",
        "obtention": moon,
        "item": leg
    },
    {
        "name": "Poignard des Âmes",
        "obtention": moon,
        "item": leg
    },
    {
        "name": "Sabre Histy",
        "obtention": moon,
        "item": leg
    },
    {
        "name": "Arc Enstone",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Calumet Happé",
        "obtention": zinit,
        "item": rar_myth_leg
    },
    {
        "name": "Cu'Hie",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Hiche",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Pêlle-Miêlle",
        "obtention": zinit,
        "item": rar_myth_leg
    },
    {
        "name": "Tronchoneuse",
        "obtention": {
            "fr": "%s IX" % ruel["fr"],
            "en":"%s IX" % ruel["en"],
            "es":"%s IX" % ruel["es"],
            "pt":"%s IX" % ruel["pt"]
        },
        "specific_name":"Plan \"Tronchoneuse\" (Souvenir)",
        "item": souv
    },
    {
        "name": "Epée d'Amakna",
        "obtention": {
            "fr": "%s d'Amakna" % mimic,
            "en": "%s of Amakna" % mimic,
            "es": "%s de Amakna" % mimic,
            "pt": "%s de Amakna" % mimic
        },
        "item": reliq
    },
    {
        "name": "Epée de Bonta",
        "obtention": {
            "fr": "%s de Bonta" % mimic,
            "en": "%s of Bonta" % mimic,
            "es": "%s de Bonta" % mimic,
            "pt": "%s de Bonta" % mimic
        },
        "item": reliq
    },
    {
        "name": "Epée de Brâkmar",
        "obtention": {
            "fr": "%s de Brâkmar" % mimic,
            "en": "%s of Brâkmar" % mimic,
            "es": "%s de Brâkmar" % mimic,
            "pt": "%s de Brâkmar" % mimic
        },
        "item": reliq
    },
    {
        "name": "Epée de Sufokia",
        "obtention": {
            "fr": "%s de Sufokia" % mimic,
            "en": "%s of Sufokia" % mimic,
            "es": "%s de Sufokia" % mimic,
            "pt": "%s de Sufokia" % mimic
        },
        "item": reliq
    },
    {
        "name": "Pelle Rieuse",
        "obtention": ombrage,
        "item": myth_leg
    },
    {
        "name": "Bottes de Klime",
        "obtention": klime,
        "item": myth
    },
    {
        "name": "Ceinture Luthuthu",
        "obtention": klime,
        "item": rare
    },
    {
        "name": "Grèves des Eclaireurs d'Otomaï",
        "obtention": {
            "fr": "%s III" % Otomai["fr"],
            "en":"%s III" % Otomai["en"],
            "es":"%s III" % Otomai["es"],
            "pt":"%s III" % Otomai["pt"]
        },
        "item":epiq
    },
    {
        "name": "L'Ami Léhunui",
        "obtention": {
            "fr": "%s III" % ruel["fr"],
            "en":"%s III" % ruel["en"],
            "es":"%s III" % ruel["es"],
            "pt":"%s III" % ruel["pt"]
        },
        "item":myth
    },
    {
        "name": "Bottes en Ivoire",
        "obtention": flaqueux,
        "item": rar_myth_leg
    },
    {
        "name": "Bottes de Passage",
        "obtention": xelorium,
        "item": myth_leg
    },
    {
        "name": "Baudrier de l'artificier",
        "obtention": trefonds,
        "item": rar_myth_leg
    },
    {
        "name": "Ceinture au Logis",
        "obtention": trefonds,
        "item": myth_leg
    },
    {
        "name": "Ceinture du Tot'",
        "obtention": trefonds,
        "item": myth_leg
    },
    {
        "name": "Bottes Féroces",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Espadrilles Egales",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Jambières à Pointes",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Kokordon",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "La Hissée Haut",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Pas-le-Sentir",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Chausses-Pèlerin d'Otomaï",
        "obtention": {
            "fr": "%s IX" % Otomai["fr"],
            "en":"%s IX" % Otomai["en"],
            "es":"%s IX" % Otomai["es"],
            "pt":"%s IX" % Otomai["pt"]
        },
        "item":reliq
    },
    {
        "name": "Bottes de N'Oeuf lieues",
        "obtention": zinit,
        "item": rar_myth_leg
    },
    {
        "name": "L'Ami Léhunui",
        "obtention": {
            "fr": "%s IX" % ruel["fr"],
            "en":"%s IX" % ruel["en"],
            "es":"%s IX" % ruel["es"],
            "pt":"%s IX" % ruel["pt"]
        },
        "specific_name":"Plan \"L'Ami Léhunui\" (Souvenir)",
        "item":souv
    },
    {
        "name": "Cape de voyage",
        "obtention": {
            "fr": "%s II" % ruel["fr"],
            "en":"%s II" % ruel["en"],
            "es":"%s II" % ruel["es"],
            "pt":"%s II" % ruel["pt"]
        },
        "item":epiq
    },
    {
        "name": "Casque Hazieff",
        "obtention": sberg,
        "specific_name": "Recette du \"Casque d’Hazieff\"",
        "item": epiq
    },
    {
        "name": "Casquaflake Impérial",
        "obtention": flaqueux,
        "item": myth_leg
    },
    {
        "name": "Cape en Ivoire",
        "obtention": flaqueux,
        "item": rar_myth_leg
    },
    {
        "name": "Col en Ivoire",
        "obtention": flaqueux,
        "item": rar_myth_leg
    },
    {
        "name": "Flakiloquente",
        "obtention": flaqueux,
        "item": rar_myth_leg
    },
    {
        "name": "La Guenille",
        "obtention": flaqueux,
        "item": rar_myth_leg
    },
    {
        "name": "L'Abracasque",
        "obtention": abraknyde,
        "item": rar_myth_leg
    },
    {
        "name": "Cape Canaverale",
        "obtention": xelorium,
        "item": rar_myth_leg
    },
    {
        "name": "Calotte Hesse",
        "obtention": moon,
        "item": rar_myth_leg
    },
    {
        "name": "Cape Rice",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Crococape",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Maskollant",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Voile Nomade",
        "obtention": moon,
        "item": myth_leg
    },
    {
        "name": "Ougalurette",
        "obtention": {
            "fr": "%s VIII" % ruel["fr"],
            "en":"%s VIII" % ruel["en"],
            "es":"%s VIII" % ruel["es"],
            "pt":"%s VIII" % ruel["pt"]
        },
        "item":reliq
    },
    {
        "name": "Capuche Draconique",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Scalp de Blérox",
        "obtention": zinit,
        "item": myth_leg
    },
    {
        "name": "Tsarine",
        "obtention": {
            "fr": "%s IX" % ruel["fr"],
            "en":"%s IX" % ruel["en"],
            "es":"%s IX" % ruel["es"],
            "pt":"%s IX" % ruel["pt"]
        },
        "specific_name": "Plan \"Tsarine\" (Souvenir)",
        "item":souv
    },
    {
        "name": "Cape Hilare",
        "obtention": ombrage,
        "item": myth_leg
    },
    {
        "name": "Dorabysse",
        "obtention": {
            "fr": "%s X" % Otomai["fr"],
            "en":"%s X" % Otomai["en"],
            "es":"%s X" % Otomai["es"],
            "pt":"%s X" % Otomai["pt"]
        },
        "item":epiq
    },
    {
        "name": "Monbow Sapin",
        "obtention": nowel,
    },
    {
        "name": [
            "Mur Chuchoté",
            "Bloc Chuchoté"
        ],
        "obtention":chuchoku,
        "specific_name": "Plan \"Murs chuchotés\"",
    },
    {
        "name": [
            "Trône d'Amakna",
            "Trône de Bonta",
            "Trône de Brâkmar",
            "Trône de Sufokia"
        ],
        "obtention":gouv,
        "specific_name": "Trônes de nation",
    },
    {
        "name": "Ragoût Bouftou aigre doux",
        "obtention": mercenaryOldBrakmar
    },
    {
        "name": "Ragoût Bouftou Croquant",
        "obtention": mercenaryOldAmakna
    },
    {
        "name": "Ragoût Bouftou Fondant",
        "obtention": mercenaryOldBonta
    },
    {
        "name": "Ragoût Bouftou Pané",
        "obtention": mercenaryOldSufokia
    },
    {
        "name": "Tofurnade aux céréales",
        "obtention": mercenaryOldAmakna
    },
    {
        "name": "Tofurnade marinée",
        "obtention": mercenaryOldSufokia
    },
    {
        "name": "Tofurnade sucrée salée",
        "obtention": mercenaryOldBonta
    },
    {
        "name": "Tofurnade épicée",
        "obtention": mercenaryOldBrakmar
    },
    {
        "name": "Brouillabaisse Sufokienne",
        "obtention": mercenaryOldSufokia
    },
    {
        "name": "Carbonage Brâkmarien",
        "obtention": mercenaryOldBrakmar
    },
    {
        "name": "Cassoulette Amaknéenne",
        "obtention": mercenaryOldAmakna
    },
    {
        "name": "Envelouté Bontarien",
        "obtention": mercenaryOldBonta
    }
]
