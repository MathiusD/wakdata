import copy
import json
import traceback
from multiprocessing import Lock, Manager, Pool, Process
from os.path import exists

from cdnwakfu.Utils.search import Dichot

from ..Items import Item
from ..JobItems import JobItem
from ..Recipes import Recipe


class Craftkfu:

    @staticmethod
    def printInMultiproc(msg: str, printLock: Lock = None):
        if printLock:
            printLock.acquire()
        try:
            print(msg)
        finally:
            if printLock:
                printLock.release()

    @classmethod
    def getCraftkfuDataFor(cls, latest: dict, id: int, item: JobItem = None, verbose: bool = False, printLock: Lock = None):
        craftkfuData = None
        if not item:
            item = JobItem.findItem(latest["jobsItems"], id)
        if item and item.title:
            craftkfuData = {}
            item.fetchData(latest)
            craftkfuData["name"] = [
                item.title.getByShortLang("fr"),
                item.title.getByShortLang("en"),
                item.title.getByShortLang("es"),
                item.title.getByShortLang("pt"),
            ]
            craftkfuData["id"] = item.definition.id
            craftkfuData["img"] = item.definition.graphicParameters.gfxId
            craftkfuData["lvlItem"] = item.definition.level
            craftkfuData["rarity"] = item.definition.rarity
            recipesData = Recipe.findRecipeByItemResultId(
                latest, item.definition.id)
            if len(recipesData) > 0:
                recipes = []
                for recipeData in recipesData:
                    recipeData.fetchData(latest)
                    recipe = []
                    for component in recipeData.recipeIngredients:
                        recipe.append({
                            "id": component.itemId,
                            "qt": component.quantity
                        })
                    recipes.append(recipe)
                    craftkfuData["lvl"] = recipeData.level
                craftkfuData["ingredients"] = recipes
                craftkfuData["xp"] = recipesData[0].getXP(
                    recipesData[0].level, latest)
                craftkfuData["upgrade"] = recipesData[0].isUpgrade
                craftkfuData["job"] = recipesData[0].recipeCategorie.id
                for result in recipesData[0].recipeResults:
                    if result.productedItemId == item.definition.id:
                        craftkfuData["nb"] = result.productedItemQuantity
            else:
                craftkfuData["ingredients"] = None
                craftkfuData["lvl"] = None
                craftkfuData["xp"] = 0
                craftkfuData["upgrade"] = False
                craftkfuData["job"] = 0
                craftkfuData["nb"] = 0
            craftkfuData["type"] = 721
        if verbose:
            cls.printInMultiproc("craftkfuData with id : %i %s build for %s." % (
                id, "has" if craftkfuData else "hasn't", latest["version"]), printLock)
        return craftkfuData

    @classmethod
    def processForAllCraftkfuData(cls, latest: dict, id: int, jobItem: JobItem = None, verbose: bool = False, targetFile: str or None = None, out: list = None, printLock: Lock = None, writeLock: Lock = None, IOLock: Lock = None):
        craftkfuData = None
        if craftkfuData is None:
            craftkfuData = cls.getCraftkfuDataFor(
                latest, id, jobItem, verbose, printLock)
        if craftkfuData is not None:
            if out is not None:
                if writeLock:
                    writeLock.acquire()
                try:
                    out.append(craftkfuData)
                    out = sorted(out, key=lambda d: d["id"])
                finally:
                    if writeLock:
                        writeLock.release()
                if targetFile:
                    if IOLock:
                        IOLock.acquire()
                    try:
                        with open(targetFile, 'w') as target:
                            json.dump(out, target, indent=4)
                    finally:
                        if writeLock:
                            writeLock.release()
            else:
                return craftkfuData

    @classmethod
    def allCraftkfuData(cls, latest: dict, id: int, verbose: bool = True):
        return cls.processForAllCraftkfuData(latest, id, verbose)

    @classmethod
    def craftkfuData(cls, latest: dict, targetFile: str = None, verbose: bool = True):
        out = cls.readLastGenOfCraftkfuData(targetFile)
        out = sorted(out, key=lambda d: d["id"])
        out = out if out else []
        print("%s values extracted from %s" % (len(out), targetFile))
        with Manager() as manager:
            out = manager.list(out)
            printLock = manager.Lock()
            writeLock = manager.Lock()
            IOLock = manager.Lock()
            dataSize = 0
            args = []
            for itemData in latest["jobsItems"]:
                jobItem = JobItem(itemData)
                dataSize += 1
                id = jobItem.definition.id
                craftkfuData = Dichot(out, id, ["id"])
                if craftkfuData:
                    print("craftkfuData with id : %i found in last generation." % id)
                else:
                    args.append((latest, id, jobItem,verbose, None, out,
                                printLock, writeLock, IOLock))
            qtOfData = len(args)
            print("Launch process for %s" % qtOfData)
            try:
                with Pool() as pool:
                    pool.starmap(cls.processForAllCraftkfuData, args)
                    pool.close()
                    pool.join()
            except (Exception, KeyboardInterrupt) as e:
                if targetFile:
                    if isinstance(e, KeyboardInterrupt):
                        print(
                            "Shutdown Craftkfu building, save building data and exit...")
                    else:
                        print(
                            "Exception occured in Craftkfu building, save building data and exit...\nException raised :\n")
                        traceback.format_exception(
                            type(e), e, e.__traceback__)
                    out = sorted(copy.deepcopy(out), key=lambda d: d["id"])
                    with open(targetFile, 'w') as target:
                        json.dump(out, target, indent=4)
                    print("%s/%s data saved in %s" % (len(out), dataSize, targetFile))
                exit(-1)
            print("%s/%s data retrieved" % (len(out), dataSize))
            out = copy.deepcopy(out)
            manager.shutdown()
        out = sorted(out, key=lambda d: d["id"])
        return out

    @classmethod
    def writeCraftkfuData(cls, latest: dict, targetFile: str, verbose: bool = True):
        out = cls.craftkfuData(latest, targetFile, verbose)
        print("%s data saved in %s" % (len(out), targetFile))
        cls.writeInFile(out, targetFile)
        return out

    @staticmethod
    def writeInFile(craftkfuData: dict, targetFile: str):
        with open(targetFile, 'w') as target:
            json.dump(craftkfuData, target, indent=4)

    @staticmethod
    def readLastGenOfCraftkfuData(targetFile: str):
        if targetFile and exists(targetFile):
            with open(targetFile, 'r') as target:
                return json.load(target)
        return []
