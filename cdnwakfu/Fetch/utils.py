import requests, os, json, rootpath
from config_wakdata import settings
from own.own_json import get_data
from .recipes import recipes_data
from ..Items import Item
from ..Recipes import Recipe
from ..Search import SearchItem
from ..Utils import Dichot

def getLatest():
    dirname = '%s/%s' % (rootpath.detect(), settings.PATH_DATA)
    path = '%s/%s' % (dirname, get_version())
    version = False
    if os.path.exists(path) != True:
        default = "0.0.0.0"
        version = default
        for file in [file for file in os.listdir(dirname) if file.count('.') == 3]:
            indice = 0
            found = False
            while indice < 4 and found == False:
                if int(version.split('.')[indice]) < int(file.split('.')[indice]):
                    version = file
                    found = True
                elif int(version.split('.')[indice]) == int(file.split('.')[indice]):
                    indice += 1
                else:
                    found = True
        path = '%s/%s' % (dirname, version) if version != default else None
    latest = get_data(path) if path else None
    if path:
        latest["version"] = version if version else get_version()
    return latest

def get_version():
    url = '%s%s' % (settings.URL_API, settings.URL_VERSION)
    version = requests.get(url).json()
    if version:
        return version["version"]
    return None

def import_data(version: str = None):
    if not version:
        version = get_version()
    if version:
        print("Start Fetch Data for version : %s" % version)
        if not os.path.exists('%s/%s/%s' % (rootpath.detect(), settings.PATH_DATA, version)):
            os.mkdir('%s/%s/%s' % (rootpath.detect(), settings.PATH_DATA, version))
        folder = '%s/building' % (rootpath.detect())
        name = "version"
        rebuild = True
        if os.path.exists(folder) and name in get_data(folder):
            with open('%s/%s.json' % (folder, name), "r") as ref:
                rebuild = False if version == json.load(ref)["version"] else True
        if rebuild:
            print("Fetch Online for rebuild")
            for name in settings.NAME_JSON:
                print("Fetch %s" % name)
                name_file = '%s/%s.json' % (version, name)
                with open('%s/%s/%s' % (rootpath.detect(), settings.PATH_DATA, name_file), "w") as target:
                        if not online(name_file, target):
                            if not local(name, target):
                                default(name, target)
            print("Rebuild...")
            latest = getLatest()
            target_folder = "./building"
            processItems(latest, target_folder, True)
            #Pour écrire le fichier ainsi produit dans le dossier building
            recipe_construction(latest, recipes_data, target_folder)
            with open('%s/version.json' % (target_folder), "w") as ref:
                json.dump({"version":version}, ref, indent=4)
        else:
            print("Data Up to date Rebuilding Ignored")
        for name in settings.NAME_JSON:
            print("Fetch %s" % name)
            name_file = '%s/%s.json' % (version, name)
            with open('%s/%s/%s' % (rootpath.detect(), settings.PATH_DATA, name_file), "w") as target:
                if not local(name, target):
                    if not online(name_file, target):
                        default(name, target)

def local(name:str, target):
    folder = '%s/building' % (rootpath.detect())
    if os.path.exists(folder) and name in get_data(folder):
        print("Dump %s" % name)
        json.dump(json.load(open(('%s/%s.json') % (folder, name))), target, indent=4)
        return True
    return False

def online(name:str, target):
    url = '%s%s' % (settings.URL_API, name)
    request = requests.get(url)
    if (request.status_code == 200):
        print("Dump %s" % name.split('/')[1].split('.')[0])
        json.dump(request.json(), target, indent=4)
        return True
    return False

def default(name:str, target):
    json.dump([], target, indent=4)
    print("Dump %s (Empty [File not Found])" % name)
    return True

def processItems(latest:dict, target_folder:str, verbose:bool = False):
    if verbose:
        lenght = len(latest["recipes"])
    for item in latest["items"]:
        index = latest["items"].index(item)
        item["recipes"] = []
        latest["items"][index] = item
    for item in latest["jobsItems"]:
        index = latest["jobsItems"].index(item)
        item["recipes"] = []
        latest["jobsItems"][index] = item
    for recip in latest["recipes"]:
        index = latest["recipes"].index(recip)
        recip["ingredients"] = []
        recip["results"] = []
        latest["recipes"][index] = recip
    for ingredient in latest["recipeIngredients"]:
        indexIngredient = latest["recipeIngredients"].index(ingredient)
        ingredient["id"] = indexIngredient
        latest["recipeIngredients"][indexIngredient] = ingredient
        recip = Dichot(latest["recipes"], ingredient["recipeId"], ["id"])
        if recip:
            index = latest["recipes"].index(recip)
            recip["ingredients"].append(indexIngredient)
            latest["recipes"][index] = recip
    for result in latest["recipeResults"]:
        indexResult = latest["recipeResults"].index(result)
        result["id"] = indexResult
        latest["recipeResults"][indexResult] = result
        recip = Dichot(latest["recipes"], result["recipeId"], ["id"])
        if recip:
            index = latest["recipes"].index(recip)
            recip["results"].append(indexResult)
            latest["recipes"][index] = recip
    for recip in latest["recipes"]:
        recipe = Recipe(recip)
        recipe.fetchData(latest, 1)
        for result in recipe.recipeResults:
            item = SearchItem.getItem(latest, result.productedItemId)
            if item:
                if isinstance(item, Item):
                    ite = Dichot(latest["items"], item._id, ['definition', 'item', 'id'])
                    index = latest["items"].index(ite)
                    ite["recipes"].append(recipe.id)
                    latest["items"][index] = ite
                else:
                    ite = Dichot(latest["jobsItems"], item._id, ['definition', 'id'])
                    index = latest["jobsItems"].index(ite)
                    ite["recipes"].append(recipe.id)
                    latest["jobsItems"][index] = ite
            else:
                print("Error")
    with open("%s/recipeResults.json" % (target_folder), 'w') as target:
        json.dump(latest["recipeResults"], target, indent=4)
    with open("%s/recipeIngredients.json" % (target_folder), 'w') as target:
        json.dump(latest["recipeIngredients"], target, indent=4)
    with open("%s/recipes.json" % (target_folder), 'w') as target:
        json.dump(latest["recipes"], target, indent=4)
    with open("%s/items.json" % (target_folder), 'w') as target:
        json.dump(latest["items"], target, indent=4)
    with open("%s/jobsItems.json" % (target_folder), 'w') as target:
        json.dump(latest["jobsItems"], target, indent=4)
    return latest

def recipe_construction(latest:dict, recipes_data:list, target_folder:str, verbose:bool=True):
    data = []
    compt = 1
    leng = len(recipes_data)
    for recipe_instruction in recipes_data:
        skip = False
        if "skip" in recipe_instruction.keys():
            if recipe_instruction["skip"] == True:
                skip = True
        if skip is not True:
            if verbose:
                print ("Data %i/%i (%s)" % (compt, leng, recipe_instruction["name"] if isinstance(recipe_instruction["name"], str) else recipe_instruction["name"][0]))
            name = None
            dat = {
                "id":compt-1,
                'recipeId':[],
                "name": name,
                "obtention": recipe_instruction["obtention"]
            }
            if isinstance(recipe_instruction["name"], str):
                raw_data = Recipe.findRecipeByItemResultName(latest, recipe_instruction["name"], "fr")
            else:
                raw_data = []
                for givenName in recipe_instruction["name"]:
                    for recipe in Recipe.findRecipeByItemResultName(latest, givenName, "fr"):
                        raw_data.append(recipe)
            for recipe in raw_data:
                recipe.fetchData(latest)
                if recipe.recipeResults[0].item:
                    if "item" in recipe_instruction.keys():
                        if isinstance(recipe.recipeResults[0].item, Item) == True:
                            rarity = recipe.recipeResults[0].item.definition.item.baseParameters.rarity
                            if rarity in recipe_instruction["item"]:
                                dat['recipeId'].append(recipe.id)
                    else:
                        dat['recipeId'].append(recipe.id)
                    if not dat["name"]:
                        dat["name"] = {
                            "fr":"Plan \"%s\"" % (getattr(recipe.recipeResults[0].item.title, "fr")),
                            "en":"\"%s\" Blueprint" % (getattr(recipe.recipeResults[0].item.title, "en")),
                            "es":"Documento: %s" % (getattr(recipe.recipeResults[0].item.title, "es")),
                            "pt":"Projecto da \"%s\"" % (getattr(recipe.recipeResults[0].item.title, "pt")),
                        }
                        if "specific_name" in recipe_instruction:
                            dat["name"]["fr"] = recipe_instruction["specific_name"]
            data.append(dat)
            if verbose:
                print("%s fetch (%i results)" % (recipe_instruction["name"], len(dat['recipeId'])))
            with open("%s/recipePattern.json" % target_folder, 'w') as target:
                if verbose:
                    print("Dump Data %i/%i" % (compt, leng))
                json.dump(data, target, indent=4)
        compt += 1