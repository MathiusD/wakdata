from ..Utils import Dichot
from ..Texts import Text
from ..Elements import UserReadableElement, CompareElement, SearchElement
from ..RecipeCategories import RecipeCategorie
from ..RecipePatterns import RecipePattern
from ..RecipeIngredients import RecipeIngredient
from ..RecipeResults import RecipeResult
from ..Search import SearchItem
from ..BaseItems import BaseItem
import math

class Recipe(UserReadableElement, CompareElement, SearchElement):
    """
    Classe représentant un recette au sein des données
    d'Ankama.
    """

    DELTA_LVL = 10
    RECIPE_BASE_XP_RATIO = 100

    def __init__(self, RecipeData:dict):
        super().__init__()
        self.id = RecipeData["id"]
        self.categoryId = RecipeData["categoryId"]
        self.level = RecipeData["level"]
        self.xpRatio = RecipeData["xpRatio"]
        self.isUpgrade = RecipeData["isUpgrade"]
        self.upgradeItemId = RecipeData["upgradeItemId"]
        self.recipeCategorie = None
        self.recipeIngredients = []
        self.recipePattern = None
        self.recipeResults = []
        self.upgradeItem = None
        self.ingredients = RecipeData["ingredients"] if "ingredients" in RecipeData else None
        self.results = RecipeData["results"] if "results" in RecipeData else None

    def fetchData(self, GameData:dict, recursive:int = 7):
        super()._recursion(recursive)
        if "recipeCategories" in GameData.keys() and not self.recipeCategorie:
            self.recipeCategorie = RecipeCategorie.findRecipeCategorie(GameData['recipeCategories'], self.categoryId)
        if "recipeIngredients" in GameData.keys() and len(self.recipeIngredients) == 0:
            self.recipeIngredients = RecipeIngredient.findRecipeIngredientsWithRecipe(GameData['recipeIngredients'], self)
        if len(self.recipeIngredients) != 0 and self._recursive:
            for ingredient in self.recipeIngredients:
                ingredient.fetchData(GameData, self._recursive)
        if "recipePattern" in GameData.keys() and not self.recipePattern:
            self.recipePattern = RecipePattern.findRecipePatternByRecipe(GameData["recipePattern"], self.id)
        if "recipeResults" in GameData.keys() and len(self.recipeResults) == 0:
            self.recipeResults = RecipeResult.findRecipeResultsWithRecipe(GameData["recipeResults"], self)
        if len(self.recipeResults) != 0 and self._recursive:
            for result in self.recipeResults:
                result.fetchData(GameData, self._recursive)
        if self.isUpgrade and "recipes" in GameData.keys() and not self.upgradeItem:
            self.upgradeItem = SearchItem.getItem(GameData, self.upgradeItemId)
        if self.upgradeItem and self._recursive:
            self.upgradeItem.fetchData(GameData, self._recursive)

    def getXP(self, crafter_level:int, GameData:dict):
        """
        Méthode permettant de déterminer l'expérience prodiguée
        lors de la fabrication de cette recette en fonction du
        niveau de l'artisant.
        """
        delta = crafter_level - (self.level + self.__class__.DELTA_LVL)
        if delta > self.__class__.DELTA_LVL:
            xp = 0
        elif delta < 0:
            xp = round(100 * self.xpRatio / self.__class__.RECIPE_BASE_XP_RATIO)
        else:
            self.fetchData(GameData, 1)
            if self.recipeCategorie:
                new_delta = self.level + self.__class__.DELTA_LVL - crafter_level
                xpRat = self.xpRatio * self.recipeCategorie.xpFactor / self.__class__.RECIPE_BASE_XP_RATIO
                xp = round(self.__class__.RECIPE_BASE_XP_RATIO / 2 * (1 + math.cos(new_delta * math.pi / self.__class__.DELTA_LVL)))
            else:
                xp = None
        return xp

    def craftRate(self, crafter_level:int):
        """
        Méthode indiquant le taux de réussite de la recette
        en fonction du niveau de l'artisant.
        """
        delta = self.level - crafter_level
        if delta > 0:
            if delta <= self.__class__.DELTA_LVL:
                rate = 1.0 - (delta * self.__class__.DELTA_LVL)/100
            else:
                rate = 0.0
        else:
            rate = 1.0
        return rate

    async def asyncGetAllComponents(self, GameData:dict):
        """
        Méthode asynchrone permettant d'extraire tous les composants irréductibles
        de la recette. Ici on appelle composant irréductible, les composants
        qui soit ne possèdent pas de recette soit ceux qui possèdent plus d'une
        recette.
        """
        return await self.__class__.asyncSearch(self.getAllComponents, (GameData))

    def getAllComponents(self, GameData:dict, out:list = None):
        """
        Méthode permettant d'extraire tous les composants irréductibles
        de la recette. Ici on appelle composant irréductible, les composants
        qui soit ne possèdent pas de recette soit ceux qui possèdent plus d'une
        recette.
        """
        components = []
        self.fetchData(GameData, 1)
        threads = []
        for ingredient in self.recipeIngredients:
            item = SearchItem.getItem(GameData, ingredient.itemId)
            if item:
                search = self.__class__.findRecipeByItemResult(GameData, item)
            else:
                search = self.__class__.findRecipeByItemResultId(GameData, ingredient.itemId)
            if search and len(search) == 1:
                self.__class__._defineThread(search[0].getAllComponents, (GameData, components), threads)
            else:
                self.__class__.UpdateComponents(components, ingredient)
        self.__class__._waitingThreads(threads)
        if out is None:
            return components
        else:
            for data in components:
                self.__class__.UpdateComponents(out, data)

    @staticmethod
    def UpdateComponents(components:list, component:RecipeIngredient):
        """
        Méthode permettant de mettre à jour une liste de composants
        avec le composant spécifié, ainsi si le composant est déjà présent
        sa quantité sera mise à jour.
        """
        update = False
        for compo in components:
            if compo.itemId == component.itemId:
                compo.quantity += component.quantity
                update = True
        if update == False:
            components.append(component)
        return components

    def compareTo(self, recipe, data=None):
        compare = super().compareTo(recipe)
        if compare:
            for attr in ["level", "xpRatio"]:
                self._compareReal(compare, attr, recipe)
            for attr in ["recipeIngredients", "recipeResults"]:
                self._compareListAttr(compare, attr, recipe)
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        for attr in ["level", "xpRatio"]:
            setattr(reverted, attr, -1 * getattr(reverted, attr))
        for attr in ["recipeIngredients", "recipeResults"]:
            for item in getattr(reverted, attr):
                item = item._reverted()
        return reverted

    def __str__(self):
        if self.recipeResults:
            if len(self.recipeResults) > 0:
                if self.recipeResults[0].item:
                    if self.recipeResults[0].item.title:
                        return self.recipeResults[0].item.title.__str__()
        return str(self.id)

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 2, with_result:bool = False):
        super()._row_sheet(lang, prefix, recursive)
        if self.recipeCategorie:
            tex = Text({"fr":"Métier","en":"Job","es":"Trabajo","pt":"Trabalho"})
            self._row.append("%s : %s" % (tex.getByShortLang(self._lang), self.recipeCategorie.title.getByShortLang(self._lang)))
        lvl = Text({"fr":"Niveau","en":"Level","es":"Nivel","pt":"Nível"})
        self._row.append("%s : %i" % (lvl, self.level))
        if self.recipePattern:
            tex = Text({"fr":"Plan Nécessaire","en":"Necessary Plan","es":"Plan necesario","pt":"Plano necessário"})
            self._row.append("%s" % (tex.getByShortLang(self._lang)))
            tex = Text({"fr":"Obtention","en":"Obtaining","es":"Obtención","pt":"Obtenção"})
            self._row.append("\t%s : %s" % (tex.getByShortLang(self._lang), self.recipePattern.obtention.getByShortLang(self._lang)))
        if len(self.recipeIngredients) != 0:
            tex = Text({"fr":"Ingrédients","en":"Ingredients","es":"Ingredientes","pt":"Ingredientes"})
            self._row.append("%s :" % tex.getByShortLang(self._lang))
            if self._recursion:
                for ingredient in self.recipeIngredients:
                    self._row.append(ingredient._row_sheet(self._lang, "\t", self._recursive))
        if len(self.recipeResults) != 0 and with_result == True:
            tex = Text({"fr":"Résultats","en":"Results","es":"Resultados","pt":"Resultados"})
            self._row.append("%s :" % tex.getByShortLang(self._lang))
            if self._recursion:
                for result in self.recipeResults:
                    self._row.append(result._row_sheet(self._lang, "\t", self._recursive))
        return {"data":self._row,"prefix":prefix}

    @classmethod
    async def asyncFindRecipe(cls, recipes:list, id:int):
        """
        Méthode asynchrone permettant de trouver une recette avec
        un id spécifique au sein de les données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipe, (recipes, id))

    @staticmethod
    def findRecipe(recipes:list, id:int):
        """
        Méthode permettant de trouver une recette avec un id spécifique
        au sein de les données d'Ankama.
        """
        search = Dichot(recipes, id, ['id'])
        return Recipe(search) if search else None

    @classmethod
    async def asyncFindRecipeByItemResultName(cls, GameData:dict, name:str, lang:str):
        """
        Méthode asynchrone permettant de trouver une recette produisant un objet
        qui correspond au nom spécifié dans la langue donnée au sein de les données
        d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeByItemResultName, (GameData, name, lang))

    @classmethod
    def findRecipeByItemResultName(cls, GameData:dict, name:str, lang:str):
        """
        Méthode permettant de trouver une recette produisant un objet qui
        correspond au nom spécifié dans la langue donnée au sein de les données
        d'Ankama.
        """
        recipes = []
        threads = []
        name = name.strip().lower()
        if "recipeResults" in GameData.keys():
            for recipe in GameData['recipes']:
                recip = Recipe(recipe)
                cls._defineThread(cls.processForSearchByItemResultName, (GameData, recip, name, lang, recipes), threads)
        cls._waitingThreads(threads)
        return recipes

    @staticmethod
    def processForSearchByItemResultName(GameData:dict, recipe, name:str, lang:str, recipes:list):
        results = RecipeResult.findRecipeResults(GameData["recipeResults"], recipe.id)
        for result in results:
            result.fetchData(GameData, 1)
            if result.item:
                if result.item.title:
                    if name in (result.item.title.getByShortLang(lang).lower().strip()):
                        recipes.append(recipe)

    @classmethod
    async def asyncFindRecipeByItemResultId(cls, GameData:dict, id:int):
        """
        Méthode asynchrone permettant de trouver une recette produisant
        un objet avec l'id donnée au sein de les données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeByItemResultId, (GameData, id))

    @classmethod
    def findRecipeByItemResultId(cls, GameData:dict, id:int):
        """
        Méthode permettant de trouver une recette produisant un objet avec
        l'id donnée au sein de les données d'Ankama.
        """
        recipes = []
        threads = []
        if "recipeResults" in GameData.keys():
            for recipe in GameData['recipes']:
                recip = Recipe(recipe)
                cls._defineThread(cls.processForItemResultId, (GameData, recip, id, recipes), threads)
        cls._waitingThreads(threads)
        return recipes

    @staticmethod
    def processForItemResultId(GameData:dict, recipe, id:int, recipes:list):
        results = RecipeResult.findRecipeResults(GameData["recipeResults"], recipe.id)
        for result in results:
            result.fetchData(GameData, 1)
            if result.item:
                if result.item._id == id:
                    recipes.append(recipe)

    @classmethod
    async def asyncFindRecipeByItemIngredientId(cls, GameData:dict, id:int):
        """
        Méthode asynchrone permettant de trouver une recette contenant parmi
        ses composants un objet avecl'id donnée au sein de les données
        d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeByItemIngredientId, (GameData, id))

    @classmethod
    def findRecipeByItemIngredientId(cls, GameData:dict, id:int):
        """
        Méthode permettant de trouver une recette contenant parmi
        ses composants un objet avecl'id donnée au sein de les données
        d'Ankama.
        """
        recipes = []
        threads = []
        if "recipeIngredients" in GameData.keys():
            for recipe in GameData['recipes']:
                recip = Recipe(recipe)
                cls._defineThread(cls.processForItemIngredientId, (GameData, recip, id, recipes), threads)
        cls._waitingThreads(threads)
        return recipes

    @staticmethod
    def processForItemIngredientId(GameData:dict, recipe, id:int, recipes:list):
        ingredients = RecipeIngredient.findRecipeIngredients(GameData["recipeIngredients"], recipe.id)
        for ingredient in ingredients:
            ingredient.fetchData(GameData, 1)
            if ingredient.item:
                if ingredient.item._id == id:
                    recipes.append(recipe)

    @classmethod
    def findRecipeByItemResult(cls, GameData:dict, item:BaseItem):
        """
        Méthode permettant de trouver une recette produisant
        l'objet ciblé au sein de les données d'Ankama.
        """
        out = []
        if item.recipes:
            for recipe in item.recipes:
                recip = cls.findRecipe(GameData["recipes"], recipe)
                if recip:
                    out.append(recip)
            return out
        else:
            return cls.findRecipeByItemResultId(GameData, item._id)
    
    @classmethod
    async def asyncFindRecipeByItemResult(cls, GameData:dict, item:BaseItem):
        """
        Méthode asynchrone permettant de trouver une recette produisant
        l'objet ciblé au sein de les données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeByItemResult, (GameData, item))