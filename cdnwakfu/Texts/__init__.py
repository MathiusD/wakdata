from .langs import Lang, defaultLang, langs, en, fr, es, pt
from .texts import Text