class Lang:

    def __init__(self, nameAttr:str, aliases:list = [], showName:str = None):
        self.nameAttr = nameAttr
        self.aliases = aliases
        self.showName = showName if showName is not None else nameAttr

    def isLangOfShortName(self, shortName:str):
        shortName = shortName.strip().lower()
        if self.nameAttr == shortName:
            return True
        else:
            for aliase in self.aliases:
                if aliase == shortName:
                    return True
        return False

    @staticmethod
    def getLangByShortName(langs:list, shortName:str):
        for lang in langs:
            if lang is not None and lang.isLangOfShortName(shortName) is True:
                return lang
        return None

    def __str__(self):
        return self.showName

en = Lang("en")
fr = Lang("fr")
es = Lang("es", ["sp"], "sp")
pt = Lang("pt")
defaultLang = en
langs = [
    en,
    fr,
    es,
    pt
]