from ..Texts import Lang, langs, defaultLang

class Text:
    """
    Classe représentant un champs de Texte
    au sein des données d'Ankama.
    Ainsi à l'heure actuelle la classe
    prend en charge les langues suivantes :
    fr, en, es, pt.
    """

    langs = langs

    @classmethod
    def shortLangs(cls):
        short = []
        for lang in cls.langs:
            short.append(lang.showName)
        return short

    @classmethod
    def shortLangsAccepted(cls):
        short = []
        for lang in cls.langs:
            short.append(lang.nameAttr)
            for aliase in lang.aliases:
                short.append(aliase)
        return short

    def __init__(self, TextData:dict):
        if TextData is None:
            raise AttributeError("Text declaration is None")
        for lang in self.__class__.langs:
            if lang.nameAttr in TextData.keys() and TextData[lang.nameAttr] not in ["", None]:
                setattr(self, lang.nameAttr, TextData[lang.nameAttr])
            else:
                for aliase in lang.aliases:
                    if aliase in TextData.keys() and TextData[aliase] not in ["", None]:
                        setattr(self, lang.nameAttr, TextData[aliase])
        if self.hasLang(defaultLang) is False:
            raise AttributeError("Default Lang is missing in Text declaration.")

    @property
    def langsOfText(self):
        out = []
        for lang in self.langs:
            if self.hasLang(lang) is True:
                out.append(lang)
        return out

    def getByShortLang(self, shortLang:str = defaultLang.nameAttr):
        lang = Lang.getLangByShortName(self.__class__.langs, shortLang)
        return self.getByLang(lang)

    def hasLang(self, lang:Lang = defaultLang):
        if lang is not None and hasattr(self, lang.nameAttr):
            return True
        return False

    def getByLang(self, lang:Lang = defaultLang):
        if self.hasLang(lang):
            return getattr(self, lang.nameAttr)
        else:
            return getattr(self, defaultLang.nameAttr)
        return None

    def __str__(self):
        return self.getByLang()