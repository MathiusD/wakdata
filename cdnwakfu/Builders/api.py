import abc
from .build import Build
from ..Recipes import Recipe
from ..RecipeIngredients import RecipeIngredient

class Api:

    provider = None
    api = None
    web = None

    @classmethod
    def website(cls):
        return "%s%s" % (cls.provider, cls.web)

    @classmethod
    def isUrlForBuildForThisApi(cls, url:str):
        return True if url.startswith(cls.website()) else False

    @classmethod
    def extractBuildFromUrl(cls, url:str):
        if cls.isUrlForBuildForThisApi(url):
            split = url.split(cls.website())
            if len(split) > 1:
                return cls.getBuild(split[1])
        return None

    @staticmethod
    @abc.abstractmethod
    def getBuild(code:str):
        pass

    @staticmethod
    @abc.abstractmethod
    def getEquipement(build:Build, latest:dict):
        pass

    @staticmethod
    @abc.abstractmethod
    def getSublimation(build:Build, latest:dict):
        pass

    @classmethod
    def getItems(cls, build:Build, latest:dict):
        items = cls.getEquipement(build, latest)
        for subli in cls.getSublimation(build, latest):
            items.append(subli)
        return items

    @classmethod
    def getComponents(cls, build:Build, latest:dict):
        components = []
        for item in cls.getItems(build, latest):
            recipes = Recipe.findRecipeByItemResult(latest, item)
            if len(recipes) == 1:
                recipe = recipes[0]
                for component in recipe.getAllComponents(latest):
                    Recipe.UpdateComponents(components, component)
            else:
                component = RecipeIngredient(
                    {
                        "recipeId":None,
                        "itemId":item._id,
                        "quantity":1,
                        "ingredientOrder":None
                    }
                )
                component.item = item
                Recipe.UpdateComponents(components, component)
        return components

    @staticmethod
    def compareComponents(firstBuild:Build, secondBuild:Build, latest:dict):
        components = []
        componentsToRemove = []
        componentsAlphaTemp = firstBuild.api.getComponents(firstBuild, latest)
        componentsBetaTemp = secondBuild.api.getComponents(secondBuild, latest)
        for componentAlpha in componentsAlphaTemp:
            for componentBeta in componentsBetaTemp:
                if componentAlpha.itemId == componentBeta.itemId:
                    newComponent = componentAlpha
                    newComponent.quantity = componentAlpha.quantity - componentBeta.quantity
                    componentsToRemove.append(componentAlpha)
                    componentsBetaTemp.remove(componentBeta)
                    components.append(newComponent)
        for componentToRemove in componentsToRemove:
            componentsAlphaTemp.remove(componentToRemove)
        for componentAlpha in componentsAlphaTemp:
            components.append(componentAlpha)
        for componentBeta in componentsBetaTemp:
            newComponent = componentBeta
            newComponent.quantity = componentBeta.quantity * -1
            components.append(newComponent)
        componentsToRemove = []
        for component in components:
            if component.quantity == 0:
                componentsToRemove.append(component)
        for componentToRemove in componentsToRemove:
            components.remove(componentToRemove)
        return components

    @staticmethod
    def getCraftkfuLink(build:Build, latest:dict):
        items = build.api.getItems(build, latest)
        first = False
        link = "https://craftkfu.waklab.fr/"
        for item in items:
            link = "%s%s%i" % (link, "?" if first is False else ",", item._id)
            if first is False:
                first = True
        return link