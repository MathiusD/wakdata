from .build import Build
from .api import Api
from ..Search import SearchItem
import requests

class SlummpApi(Api):

    provider = "http://slummp.ddns.net/wakfubuilder/"
    api = "load.php"
    web = "?build="

    @classmethod
    def getBuild(cls, code:str):
        response = requests.post("%s%s" % (cls.provider, cls.api), data={"id":code, "keys":"true"})
        json = response.json()
        if json:
            return Build(json, code, cls)
        return None

    @staticmethod
    def getEquipement(build:Build, latest:dict):
        items = []
        if "items" in build.row.keys():
            for item in build.row["items"]:
                id = item["id"]
                if id != 0:
                    ite = SearchItem.getItem(latest, id)
                    items.append(ite)
        return items

    @staticmethod
    def getSublimation(build:Build, latest:dict):
        return []
        #Sublimation non implémenté dans ce builder