from .build import Build
from .api import Api
from ..Search import SearchItem
from http import HTTPStatus
import requests

class ZenithApi(Api):

    provider = "https://zenithwakfu.com/"
    apiProvider = "https://api.zenithwakfu.com/"
    api = "api/build/data/"
    web = "builder/"

    @classmethod
    def getBuild(cls, code:str):
        response = requests.get("%s%s%s" % (cls.apiProvider, cls.api, code))
        if response.status_code == HTTPStatus.OK:
            json = response.json()
            if json:
                return Build(json, code, cls)
        return None

    @staticmethod
    def getEquipement(build:Build, latest:dict):
        items = []
        if "items" in build.row.keys():
            for id in build.row["items"]:
                ite = SearchItem.getItem(latest, id)
                items.append(ite)
        return items

    @staticmethod
    def getSublimation(build:Build, latest:dict):
        subli = []
        if "sublimations" in build.row.keys():
            for id in build.row["sublimations"]:
                ite = SearchItem.getItem(latest, id)
                subli.append(ite)
        return subli