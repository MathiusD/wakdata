class BaseParameters:
    """
    Classe représentant les paramètres de base
    d'un objet dans les données d'Ankama.
    """

    def __init__(self, BaseParametersData:dict):
        self.itemTypeId = BaseParametersData["itemTypeId"]
        self.itemSetId = BaseParametersData["itemSetId"]
        self.rarity = BaseParametersData["rarity"]
        self.bindType = BaseParametersData["bindType"]
        self.minimumShardSlotNumber = BaseParametersData["minimumShardSlotNumber"]
        self.maximumShardSlotNumber = BaseParametersData["maximumShardSlotNumber"]