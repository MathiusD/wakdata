class UseParameters:
    """
    Classe représentant les statistiques d'usage
    d'un effet au sein des données d'Ankama.
    """

    def __init__(self, UseParametersData:dict):
        self.useCostAp = UseParametersData["useCostAp"]
        self.useCostMp = UseParametersData["useCostMp"]
        self.useCostWp = UseParametersData["useCostWp"]
        self.useRangeMin = UseParametersData["useRangeMin"]
        self.useRangeMax = UseParametersData["useRangeMax"]
        self.useTestFreeCell = UseParametersData["useTestFreeCell"]
        self.useTestLos = UseParametersData["useTestLos"]
        self.useTestOnlyLine = UseParametersData["useTestOnlyLine"]
        self.useTestNoBorderCell = UseParametersData["useTestNoBorderCell"]
        self.useWorldTarget = UseParametersData["useWorldTarget"]