class ShardsParameters:
    """
    Classe représentant le runage au sein
    des données d'Ankama.
    """

    def __init__(self, ShardsParametersData:dict):
        self.color = ShardsParametersData["color"]
        self.doubleBonusPosition = ShardsParametersData["doubleBonusPosition"]
        self.shardLevelingCurve = ShardsParametersData["shardLevelingCurve"]
        self.shardLevelRequirement = ShardsParametersData["shardLevelRequirement"]