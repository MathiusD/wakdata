class GraphicParameters:
    """
    Classe représentant les paramètres graphiques
    dans les données d'Ankama.
    """

    def __init__(self, GraphicParametersData:dict):
        self.gfxId = GraphicParametersData["gfxId"]
        self.femaleGfxId = GraphicParametersData["femaleGfxId"]

    @property
    def uri(self):
        """
        Propriété menant vers l'asset de l'objet
        sur les serveurs d'Ankama.
        """
        return self.constructUri()

    def constructUri(self, size:int=115):
        """
        Propriété menant vers l'asset de l'objet
        sur les serveurs d'Ankama avec la taille ciblée.
        """
        return "https://static.ankama.com/wakfu/portal/game/item/%i/%s.png" % (size, self.gfxId)
    
    @property
    def vertyloUri(self):
        """
        Propriété menant vers l'asset de l'objet sur
        le dépôt de vertylo.
        """
        return "https://vertylo.github.io/wakassets/items/%s.png" % (self.gfxId)

    @property
    def femaleUri(self):
        """
        Propriété menant vers l'asset de l'objet
        en version féminine sur les serveurs d'Ankama.
        """
        return self.femaleConstructUri()

    def femaleConstructUri(self, size:int=115):
        """
        Propriété menant vers l'asset de l'objet
        en version féminine sur les serveurs d'Ankama
        avec la taille ciblée.
        """
        return "https://static.ankama.com/wakfu/portal/game/item/%i/%s.png" % (size, self.femaleGfxId)
    
    @property
    def femaleVertyloUri(self):
        """
        Propriété menant vers l'asset de l'objet
        en version féminine sur le dépôt de vertylo.
        """
        return "https://vertylo.github.io/wakassets/items/%s.png" % (self.femaleGfxId)