from ..Texts import Text
from ..Elements import DataElement, CompareElement, SearchElement
from ..Resources import Resource

class CollectibleResource(DataElement, CompareElement, SearchElement):
    """
    Classe contenant diverses données sur
    l'interaction avec une resource.
    """

    def __init__(self, CollectibleResourceData:dict):
        super().__init__()
        self.id = CollectibleResourceData["id"]
        self.skillId = CollectibleResourceData["skillId"]
        self.resourceId = CollectibleResourceData["resourceId"]
        self.resourceIndex = CollectibleResourceData["resourceIndex"]
        self.collectItemId = CollectibleResourceData["collectItemId"]
        self.resourceNextIndex = CollectibleResourceData["resourceNextIndex"]
        self.skillLevelRequired = CollectibleResourceData["skillLevelRequired"]
        self.simultaneousPlayer = CollectibleResourceData["simultaneousPlayer"]
        self.visualFeedbackId = CollectibleResourceData["visualFeedbackId"]
        self.duration = CollectibleResourceData["duration"]
        self.mruOrder = CollectibleResourceData["mruOrder"]
        self.xpFactor = CollectibleResourceData["xpFactor"]
        self.collectLootListId = CollectibleResourceData["collectLootListId"]
        self.collectConsumableItemId = CollectibleResourceData["collectConsumableItemId"]
        self.collectGfxId = CollectibleResourceData["collectGfxId"]
        self.displayInCraftDialog = CollectibleResourceData["displayInCraftDialog"]
        self.resource = None

    def fetchData(self, GameData:dict, recursive:int = 3):
        super()._recursion(recursive)
        if "resources" in GameData.keys() and not self.resource:
            self.resource = Resource.findResource(GameData["resources"], self.resourceId)
        if self.resource and self._recursive:
            self.resource.fetchData(GameData, self._recursive)

    def compareTo(self, collectibleResource, data=None):
        compare = super().compareTo(collectibleResource)
        if compare and self.resourceId == collectibleResource.resourceId and self.collectItemId == collectibleResource.collectItemId:
            for attr in ["resourceIndex", "resourceNextIndex", "skillLevelRequired", "simultaneousPlayer", "duration", "xpFactor"]:
                self._compareReal(compare, attr, collectibleResource)
            for attr in ["skillId", "visualFeedbackId", "mruOrder", "collectLootListId", "collectConsumableItemId", "collectGfxId", "displayInCraftDialog"]:
                setattr(compare, attr, None)
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        for attr in ["resourceIndex", "resourceNextIndex", "skillLevelRequired", "simultaneousPlayer", "duration", "xpFactor"]:
            setattr(reverted, attr, -1 * getattr(reverted, attr))
        return reverted
    
    def __str__(self):
        return "CollectibleResource for %s" % self.resource.__str__() if self.resource else str(self.id)

    @staticmethod
    def findCollectibleResources(collectibleResources:list, id:int):
        data = []
        for collectibleResource in collectibleResources:
            if collectibleResource['collectItemId'] == id:
                data.append(CollectibleResource(collectibleResource))
        return data
    
    @classmethod
    async def asyncCollectibleResources(cls, collectibleResources:list, id:int):
        return await cls.asyncSearch(cls.findCollectibleResources, (collectibleResources, id))