from ...Parameters import BaseParameters, GraphicParameters, UseParameters, ShardsParameters
from ...ItemProperties import ItemPropertie
from ...EquipementItemTypes import EquipementItemType
from ...Raritys import Rarity
from ...Texts import Text
from ...Elements import UserReadableElement, CompareElement

class ItemDefinitionItem(UserReadableElement, CompareElement):
    """
    Classe comportant des données liées
    à un ItemDefinition dans les données d'Ankama.
    """

    def __init__(self, ItemDefinitionItemData:dict):
        super().__init__()
        self.id = ItemDefinitionItemData["id"]
        self.level = ItemDefinitionItemData["level"]
        self.baseParameters = BaseParameters(ItemDefinitionItemData["baseParameters"])
        self.useParameters = UseParameters(ItemDefinitionItemData["useParameters"])
        self.graphicParameters = GraphicParameters(ItemDefinitionItemData["graphicParameters"])
        if "shardsParameters" in ItemDefinitionItemData.keys():
            self.shardsParameters = ShardsParameters(ItemDefinitionItemData["shardsParameters"])
        else:
            self.shardsParameters = None
        self._properties = ItemDefinitionItemData["properties"]
        self.equipmentItemType = None
        self.properties = []

    def fetchData(self, GameData:dict):
        if "equipmentItemTypes" in GameData.keys() and not self.equipmentItemType:
            self.equipmentItemType = EquipementItemType.findEquipmentItemType(GameData['equipmentItemTypes'], self.baseParameters.itemTypeId)
        if "itemProperties" in GameData.keys() and len(self.properties) == 0:
            self.properties = []
            for prop in self._properties:
                self.properties.append(ItemPropertie.findPropertie(GameData["itemProperties"], prop))

    def compareTo(self, itemDefinitionItem, data=None):
        compare = super().compareTo(itemDefinitionItem)
        if compare:
            compare = self._compareReal(compare, "level", itemDefinitionItem)
            compare.equipmentItemType = None
            compare.baseParameters.rarity = None
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        for attr in ["level"]:
            setattr(reverted, attr, -1 * getattr(reverted, attr))
        return reverted

    def _row_sheet(self, lang='fr', prefix='', recursive=1):
        super()._row_sheet(lang, prefix, recursive)
        if self.baseParameters.rarity:
            rarity = Text({"fr":"Rareté", "en":"Rarity", "es":"Rareza", "pt":"Raridade"})
            msg = "%s : %s" % (rarity.getByShortLang(lang), Rarity.RaritytoText(self.baseParameters.rarity).getByShortLang(lang))
            self._row.append(msg)
        if self.equipmentItemType:
            equipementType = Text({"fr":"Type", "en":"Type", "es":"Escriba", "pt":"Datilografar"})
            msg = "%s : %s" % (equipementType.getByShortLang(lang), self.equipmentItemType.title.getByShortLang(lang))
            self._row.append(msg)
        return {"data":self._row,"prefix":prefix}