from ...Elements import UserReadableElement, CompareElement
from ...Texts import Text
from ..ItemDefinitions import ItemDefinitionItem
from ...Effects import Effect
from ...Utils import sheet_process

class ItemDefinition(UserReadableElement, CompareElement):
    """
    Classe comportant des données liées
    à un Item dans les données d'Ankama.
    """

    def __init__(self, ItemDefinitionData:dict):
        super().__init__()
        self.item = ItemDefinitionItem(ItemDefinitionData["item"])
        self.useEffects = []
        for effect in ItemDefinitionData["useEffects"]:
            if "subEffects" in effect:
                self.useEffects.append(Effect(effect["effect"], effect["subEffects"]))
            else:
                self.useEffects.append(Effect(effect["effect"]))
        self.useCriticalEffects = []
        for effect in ItemDefinitionData["useCriticalEffects"]:
            if "subEffects" in effect:
                self.useCriticalEffects.append(Effect(effect["effect"], effect["subEffects"]))
            else:
                self.useCriticalEffects.append(Effect(effect["effect"]))
        self.equipEffects = []
        for effect in ItemDefinitionData["equipEffects"]:
            if "subEffects" in effect:
                self.equipEffects.append(Effect(effect["effect"], effect["subEffects"]))
            else:
                self.equipEffects.append(Effect(effect["effect"]))

    def fetchData(self, GameData:dict, recursive:int = 4):
        super()._recursion(recursive)
        if self._recursive:
            self.item.fetchData(GameData)
            for effect in self.useEffects:
                effect.fetchData(GameData, self.item.level)
            for effect in self.useCriticalEffects:
                effect.fetchData(GameData, self.item.level)
            for effect in self.equipEffects:
                effect.fetchData(GameData, self.item.level)

    def compareTo(self, itemDefinition, data=None):
        compare = super().compareTo(itemDefinition)
        if compare:
            compare.item = self.item.compareTo(itemDefinition.item)
            lvls = [self.item.level, itemDefinition.item.level]
            for attr in ["useEffects", "useCriticalEffects", "equipEffects"]:
                compare = self._compareListAttr(compare, attr, itemDefinition, lvls)
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        for attr in ["useEffects", "useCriticalEffects", "equipEffects"]:
            for effect in getattr(reverted, attr):
                effect = effect._reverted()
        return reverted

    def sheet(self, lang:str = "fr", prefix:str = "", without_stats:bool = False):
        return sheet_process(self._row_sheet(lang, prefix, without_stats=without_stats))

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 4, without_stats:bool = False):
        super()._row_sheet(lang, prefix, recursive)
        self._row.append(self.item._row_sheet(self._lang, "", self._recursive))
        if without_stats is False:
            if len(self.useEffects) != 0 and self._recursive:
                texuse = Text({"fr":"Effets à l'usage","en":"Effects in use","es":"Efectos en el uso","pt":"Efeitos em uso"})
                self._row.append("%s :" % texuse.getByShortLang(self._lang))
                for effect in self.useEffects:
                    self._row.append(effect._row_sheet(self._lang, "\t", self._recursive))
            if len(self.useCriticalEffects) != 0 and self._recursive:
                texuscrit = Text({"fr":"Effets à l'usage (Critique)","en":"Effects in use (Critical)","es":"Efectos en el uso (Crítico)","pt":"Efeitos em uso (Crítico)"})
                self._row.append("%s :" % texuscrit.getByShortLang(self._lang))
                for effect in self.useCriticalEffects:
                    self._row.append(effect._row_sheet(self._lang, "\t", self._recursive))
            if len(self.equipEffects) != 0 and self._recursive:
                texequip = Text({"fr":"Effets à l'Equipement","en":"Effects on Equipment","es":"Efectos en el equipo","pt":"Efeitos no equipamento"})
                self._row.append("%s :" % texequip.getByShortLang(self._lang))
                for effect in self.equipEffects:
                    self._row.append(effect._row_sheet(self._lang, "\t", self._recursive))
        return {"data":self._row,"prefix":prefix}