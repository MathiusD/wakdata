from ..Utils import Dichot, Analyse, ComparatorStr
from ..EquipementItemTypes import EquipementItemType
from ..Texts import Text
from ..Raritys import Rarity
from .ItemDefinitions import ItemDefinition
from ..BaseItems import BaseItem
from ..Criterias import Criteria, ActionCriteria, EquipementItemTypeCriteria, LevelCriteria, RarityCriteria
from ..Utils import sheet_process

class Item(BaseItem):
    """
    Classe représentant un objet au sein des données d'Ankama.
    """

    def __init__(self, ItemData:dict):
        self.definition = ItemDefinition(ItemData["definition"])
        if "description" in ItemData:
            try:
                self.description = Text(ItemData["description"])
            except AttributeError:
                self.description = Text({
                    "fr":"Description Inconnue" if ItemData["description"]["fr"] in [None, ""] else ItemData["description"]["fr"],
                    "en":"Unknow Description"
                })
        else:
            self.description = None
        try:
            title = Text(ItemData["title"]) if "title" in ItemData else None
        except AttributeError:
            title = Text({
                "fr":"Item Inconnu" if ItemData["title"]["fr"] in [None, ""] else ItemData["title"]["fr"],
                "en":"Unknow Item"
            })
        super().__init__(self.definition.item.id, title, self.definition.item.graphicParameters, ItemData["recipes"] if "recipes" in ItemData else None)

    @property
    def encyclopediaLink(self):
        provider = "https://www.wakfu.com/"
        if self.definition and self.definition.item and self.definition.item.equipmentItemType:
            position = self.definition.item.equipmentItemType.definition.equipmentPositions
            folder = None
            if len(position) > 0:
                pos = position[0]
                if pos.endswith("WEAPON"):
                    folder = Text({
                        "fr":"armes",
                        "en":"weapons",
                        "es":"armas",
                        "pt":"armas"
                    })
                elif pos == "ACCESSORY":
                    folder = Text({
                        "fr":"accessoires",
                        "en":"accessories",
                        "es":"accesorios",
                        "pt":"acessorios"
                    })
                elif pos == "COSTUME":
                    folder = Text({
                        "fr":"personnalisation",
                        "en":"customization",
                        "es":"personalizacion",
                        "pt":"personalizacao"
                    })
                else:
                    folder = Text({
                        "fr":"armures",
                        "en":"armors",
                        "es":"armaduras",
                        "pt":"armaduras"
                    })
            if folder:
                return Text({
                    "fr":"%sfr/mmorpg/encyclopedie/%s/%i" % (provider, folder.fr, self._id),
                    "en":"%sen/mmorpg/encyclopedia/%s/%i" % (provider, folder.en, self._id),
                    "es":"%ses/mmorpg/enciclopedia/%s/%i" % (provider, folder.es, self._id),
                    "pt":"%spt/mmorpg/enciclopedia/%s/%i" % (provider, folder.pt, self._id)
                })
        return None

    def _returnJson(self, items:list):
        return Dichot(items, self._id, ['definition', 'item', 'id'])

    def fetchData(self, GameData:dict, recursive:int = 5):
        super().fetchData(GameData, recursive)
        if self._recursive:
            self.definition.fetchData(GameData, self._recursive)

    def __str__(self):
        return self.title.__str__()

    def sheet(self, lang:str = "fr", prefix:str = "", without_stats:bool = False):
        return sheet_process(self._row_sheet(lang, prefix, without_stats=without_stats))

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 5, without_stats:bool = False):
        super()._row_sheet(lang, prefix, recursive)
        jonclvl = Text({"fr":"Niveau","en":"Level","es":"Nivel","pt":"Nível"})
        self._row.append("%s : %i" % (jonclvl.getByShortLang(self._lang) ,self.definition.item.level))
        joncdesc = Text({"fr":"Description","en":"Description","es":"Descripción","pt":"Descrição"})
        if self.description:
            self._row.append("%s : %s" % (joncdesc.getByShortLang(self._lang), self.description.getByShortLang(self._lang)))
        self._row.append(self.definition._row_sheet(self._lang, recursive=self._recursive, without_stats=without_stats))
        return {"data":self._row,"prefix":prefix}

    def compareTo(self, item, data=None):
        compare = super().compareTo(item)
        if compare:
            compare.description = None
            compare.definition = self.definition.compareTo(item.definition)
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        reverted.defintion = reverted.definition._reverted()
        return reverted

    @classmethod
    def findItemsByName(cls, items:list, name:str, lang:str):
        """
        Méthode permettant de rechercher tout les Items
        correspondant au nom dans la langue donnée.
        """
        out = []
        threads = []
        name = name.strip().lower()
        for item in items:
            ite = Item(item)
            cls._defineThread(cls.processForName, (ite, name, lang, out), threads)
        cls._waitingThreads(threads)
        return out

    @classmethod
    async def asyncFindItemsByName(cls, items:list, name:str, lang:str):
        """
        Méthode asynchrone permettant de rechercher tout les Items
        correspondant au nom dans la langue donnée.
        """
        return await cls.asyncSearch(cls.findItemsByName, (items, name, lang))

    @staticmethod
    def processForName(item, name:str, lang:str, out:list):
        if item.title:
            if name in (item.title.getByShortLang(lang).lower().strip()):
                out.append(item)
    
    @classmethod
    def findItemsWithCriteria(cls, GameData:dict, criteria:Criteria):
        """
        Méthode permettant de rechercher tout les Items correspondant
        au critère spécifié au sein des données d'Ankama.
        """
        out = []
        threads = []
        if isinstance(criteria, ActionCriteria) or isinstance(criteria, EquipementItemTypeCriteria) or isinstance(criteria, LevelCriteria) or isinstance(criteria, RarityCriteria):
            for item in GameData["items"]:
                ite = Item(item)
                cls._defineThread(cls.processForCriteria, (GameData, ite, criteria, out), threads)
            cls._waitingThreads(threads)
        return out

    @classmethod
    async def asyncFindItemsWithCriteria(cls, GameData:dict, criteria:Criteria):
        """
        Méthode asyhnchorne permettant de rechercher tout les Items
        correspondant au critère spécifié au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItemsWithCriteria, (GameData, criteria))

    @staticmethod
    def processForCriteria(GameData:dict, item, criteria:Criteria, out:list):
        item.fetchData(GameData)
        if isinstance(criteria, ActionCriteria):
            effects = ["useEffects", "useCriticalEffects", "equipEffects"]
            for searchEffect in effects:
                for oneEffect in getattr(item.definition, searchEffect):
                    if criteria.isMatched(oneEffect, item.definition.item.level) and not (item in out):
                        out.append(item)
        elif isinstance(criteria, EquipementItemTypeCriteria):
            if item.definition.item.equipmentItemType and criteria.isMatched(item.definition.item.equipmentItemType):
                out.append(item)
        elif isinstance(criteria, LevelCriteria):
            if criteria.isMatched(item.definition.item.level):
                out.append(item)
        elif isinstance(criteria, RarityCriteria):
            if criteria.isMatched(item.definition.item.baseParameters.rarity):
                out.append(item)

    @classmethod
    async def asyncFindItemsWithCriterias(cls, GameData:dict, criterias:list):
        """
        Méthode asyhnchorne permettant de rechercher tout les Items
        correspondant aux critères spécifiés au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItemsWithCriterias, (GameData, criterias))

    @classmethod
    def findItemsWithCriterias(cls, GameData:dict, criterias:list):
        """
        Méthode permettant de rechercher tout les Items correspondant
        aux critères spécifiés au sein des données d'Ankama.
        """
        if len(criterias) > 0:
            temp = cls.findItemsWithCriteria(GameData, criterias[0])
            if len(criterias) > 1 and len(temp) > 0:
                GameTemp = GameData.copy()
                GameTemp["items"] = []
                for item in temp:
                    GameTemp["items"].append(item._returnJson(GameData["items"]))
                return cls.findItemsWithCriterias(GameTemp, criterias[1:])
            else:
                return temp
        return []

    @classmethod
    async def asyncFindItemsByAction(cls, GameData:dict, action:int):
        """
        Méthode asynchrone permettant de rechercher tout les Items correspondant
        à l'action avec l'id spécifié au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItemsByAction, (GameData, action))

    @classmethod
    def findItemsByAction(cls, GameData:dict, action:int):
        """
        Méthode permettant de rechercher tout les Items correspondant
        à l'action avec l'id spécifié au sein des données d'Ankama.
        """
        return cls.findItemsWithCriteria(GameData, ActionCriteria(action))

    @classmethod
    async def asyncFindItemsByActions(cls, GameData:dict, actions:list):
        """
        Méthode asynchrone permettant de rechercher tout les Items correspondant
        aux actions avec les id spécifiés au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItemsByActions, (GameData, actions))

    @classmethod
    def findItemsByActions(cls, GameData:dict, actions:list):
        """
        Méthode permettant de rechercher tout les Items correspondant
        aux actions avec les id spécifiés au sein des données d'Ankama.
        """
        data = []
        for action in actions:
            data.append(ActionCriteria(action))
        return cls.findItemsWithCriterias(GameData, data)

    @classmethod
    async def asyncFindItemsByEquipementItemType(cls, GameData:dict, equipementItemTypeId:list):
        """
        Méthode asynchrone permettant de rechercher tout les Items correspondant
        aux type d'équipement spécifié au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItemByEquipementItemType, (GameData, equipementItemTypeId))

    @classmethod
    def findItemByEquipementItemType(cls, GameData:dict, equipementItemTypeId:int):
        """
        Méthode permettant de rechercher tout les Items correspondant
        aux type d'équipement spécifié au sein des données d'Ankama.
        """
        return cls.findItemsWithCriteria(GameData, EquipementItemTypeCriteria(equipementItemTypeId))

    @classmethod
    async def asyncFindItemByLevel(cls, GameData:dict, level:int, requestOperator:str = None):
        """
        Méthode asynchrone permettant de rechercher tout les Items correspondant
        au niveau spécifié au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItemByLevel, (GameData, level, requestOperator))

    @classmethod
    def findItemByLevel(cls, GameData:dict, level:int, requestOperator:str = None):
        """
        Méthode permettant de rechercher tout les Items correspondant
        au niveau spécifié au sein des données d'Ankama.
        """
        return cls.findItemsWithCriteria(GameData, LevelCriteria(level, requestOperator))

    @classmethod
    async def asyncFindItemByRarity(cls, GameData:dict, rarity:Rarity):
        """
        Méthode asynchrone permettant de rechercher tout les Items correspondant
        à la rareté spécifiée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItemByRarity, (GameData, rarity))

    @classmethod
    def findItemByRarity(cls, GameData:dict, rarity:Rarity):
        """
        Méthode permettant de rechercher tout les Items correspondant
        à la rareté spécifiée au sein des données d'Ankama.
        """
        return cls.findItemsWithCriteria(GameData, RarityCriteria(rarity))

    @classmethod
    async def asyncFindItem(cls, items:list, id:int):
        """
        Méthode asynchrone permettant de rechercher l'Item correspondant
        à l'id spécifiée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findItem, (items, id))

    @staticmethod
    def findItem(items:list, id:int):
        """
        Méthode permettant de rechercher l'Item correspondant
        à l'id spécifiée au sein des données d'Ankama.
        """
        search = Dichot(items, id, ['definition', 'item', 'id'])
        return Item(search) if search else None