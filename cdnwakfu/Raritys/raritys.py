from enum import Enum
from ..Texts import Text

class Rarity(Enum):
    """
    Classe représentant les raretés
    au sein des données d'Ankama.
    """
    Commun = 0
    Inhabituel = 1
    Rare = 2
    Mythique = 3
    Légendaire = 4
    Relique = 5
    Souvenir = 6
    Epique = 7
    Admin = 10

    def __str__(self):
        return Rarity.RaritytoText(self.value).__str__()

    @staticmethod
    def RaritytoText(rarity:int):
        """
        Méthode permettant à partir d'un indice de
        rareté d'en obtenir le nom.
        """
        if rarity == 0:
            return Text({"fr":"Commun","en":"Common","es":"Común","pt":"Comum"})
        elif rarity == 1:
            return Text({"fr":"Inhabituel","en":"Unusual","es":"Inusual","pt":"Excepcional"})
        elif rarity == 2:
            return Text({"fr":"Rare","en":"Rare","es":"Raro","pt":"Raro"})
        elif rarity == 3:
            return Text({"fr":"Mythique","en":"Mythical","es":"Mítico","pt":"Mítico"})
        elif rarity == 4:
            return Text({"fr":"Légendaire","en":"Legendary","es":"Legendario","pt":"Lendário"})
        elif rarity == 5:
            return Text({"fr":"Relique","en":"Relic","es":"Reliquia","pt":"Relíquia"})
        elif rarity == 6:
            return Text({"fr":"Souvenir","en":"Memory","es":"Memoria","pt":"Memória"})
        elif rarity == 7:
            return Text({"fr":"Epique","en":"Epic","es":"Épico","pt":"Epopeia"})
        elif rarity == 10:
            return Text({"fr":"Admin","en":"Admin","es":"Admin","pt":"Admin"})
        else:
            return Text({"fr":"Rareté non Connue", "en":"Rarity not Known", "es":"Rareza desconocida", "pt":"Raridade não conhecida"})
