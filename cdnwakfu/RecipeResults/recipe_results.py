from ..ContainItems import ContainItem
from ..Utils import Dichot

class RecipeResult(ContainItem):
    """
    Classe représentant le résultat d'une recette
    au sein des données d'Ankama.
    """

    def __init__(self, RecipeResultData:dict):
        self.recipeId = RecipeResultData["recipeId"]
        self.productedItemId = RecipeResultData["productedItemId"]
        self.productOrder = RecipeResultData["productOrder"]
        self.productedItemQuantity = RecipeResultData["productedItemQuantity"]
        super().__init__(self.productedItemId, self.productedItemQuantity, RecipeResultData["id"] if "id" in RecipeResultData else None)

    @classmethod
    async def asyncFindRecipeResults(cls, recipeResults:list, id:int):
        """
        Méthode asynchrone permettant de rechercher le résultat
        d'une recette avec l'id donnée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeResults, (recipeResults, id))

    @staticmethod
    def findRecipeResults(recipeResults:dict, id:int):
        """
        Méthode permettant de rechercher le résultat d'une recette
        avec l'id donnée au sein des données d'Ankama.
        """
        results = []
        for result in recipeResults:
            if result['recipeId'] == id:
                results.append(RecipeResult(result))
        return results

    @classmethod
    async def asyncFindRecipeResultsWithRecipe(cls, recipeResults:list, recipe):
        """
        Méthode asynchrone permettant de rechercher le résultat
        d'une recette donnée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeResultsWithRecipe, (recipeResults, recipe))

    @classmethod
    def findRecipeResultsWithRecipe(cls, recipeResults:dict, recipe):
        """
        Méthode permettant de rechercher le résultat d'une recette
        avec la recette donnée au sein des données d'Ankama.
        """
        if recipe.results:
            results = []
            for result in recipe.results:
                search = Dichot(recipeResults, result, ["id"])
                if search:
                    results.append(RecipeResult(search))
            return results
        else:
            return cls.findRecipeResults(recipeResults, recipe.id)