class EquipementItemTypeDefinition:
    """
    Classe comportant des données liées
    à un EquipementItemType dans les données d'Ankama.
    """
    
    def __init__(self, EquipementItemTypeDefinitionData:dict):
        self.id = EquipementItemTypeDefinitionData["id"]
        self.parentId = EquipementItemTypeDefinitionData["parentId"]
        self.equipmentPositions = EquipementItemTypeDefinitionData["equipmentPositions"]
        self.equipmentDisabledPositions = EquipementItemTypeDefinitionData["equipmentDisabledPositions"]
        self.isRecyclable = EquipementItemTypeDefinitionData["isRecyclable"]
        self.isVisibleInAnimation = EquipementItemTypeDefinitionData["isVisibleInAnimation"]