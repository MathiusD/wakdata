from ..Utils import Dichot
from ..Texts import Text
from ..EquipementItemTypes import EquipementItemTypeDefinition
from ..Elements import SearchElement

class EquipementItemType(SearchElement):
    """
    Classe représentant un type d'équipement
    au sein des données d'Ankama.
    """

    def __init__(self, EquipementItemTypeData:dict):
        try:
            self.title = Text(EquipementItemTypeData["title"])
        except AttributeError:
            self.title = Text({
                "fr":"Type d'Equipement Inconnu" if EquipementItemTypeData["title"]["fr"] in [None, ""] else EquipementItemTypeData["title"]["fr"],
                "en":"Unknow Equipement Type"
            })
        self.definition = EquipementItemTypeDefinition(EquipementItemTypeData["definition"])

    def __str__(self):
        return self.title.__str__()

    @staticmethod
    def findEquipmentItemType(equipmentItemTypes:list, id:int):
        """
        Méthode permettant de trouver le type d'équipement donné
        avec son id au sein des données d'Ankama.
        """
        search = Dichot(equipmentItemTypes, id, ['definition', 'id'])
        return EquipementItemType(search) if search else None

    @classmethod
    async def asyncFindEquipementItemType(cls, equipmentItemTypes:list, id:int):
        """
        Méthode asynchrone permettant de trouver le type
        d'équipement donné avec son id au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findEquipmentItemType, (equipmentItemTypes, id))
