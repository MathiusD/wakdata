from ..Utils import Dichot
from ..Texts import Text
from ..Elements import SearchElement, UserReadableElement

class ResourceType(SearchElement, UserReadableElement):
    """
    Classe représentant un type de ressource au
    sein des données d'Ankama.
    """

    def __init__(self, ResourceTypeData:dict):
        self.id = ResourceTypeData["definition"]["id"]
        self.affectWakfu = ResourceTypeData["definition"]["affectWakfu"]
        try:
            self.title = Text(ResourceTypeData["title"])
        except AttributeError:
            self.title = Text({
                "fr":"Type de Ressource Inconnu" if ResourceTypeData["title"]["fr"] in [None, ""] else ResourceTypeData["title"]["fr"],
                "en":"Unknow Resource Type"
            })

    def __str__(self):
        return self.title.__str__()

    def sheet(self, lang:str = "fr", prefix:str = ""):
        lang = lang.lower().strip()
        return "%s%s" % (prefix, self.title.getByShortLang(lang))

    @classmethod
    async def asyncFindResourceType(cls, resource:list, id:int):
        """
        Méthode asynchrone permettant de rechercher le type de
        ressource avec l'id donné au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findResourceType, (resource, id))

    @staticmethod
    def findResourceType(resource:list, id:int):
        """
        Méthode permettant de rechercher le type de ressource
        avec l'id donné au sein des données d'Ankama.
        """
        search = Dichot(resource, id, ['definition', 'id'])
        return ResourceType(search) if search else None