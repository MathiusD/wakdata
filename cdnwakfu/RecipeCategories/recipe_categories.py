from ..Utils import Dichot
from ..Texts import Text
from ..Elements import SearchElement

class RecipeCategorie(SearchElement):
    """
    Classe représentant la catégorie d'une recette
    au sein des données d'Ankama.
    """

    def __init__(self, RecipeCategorieData:dict):
        self.id = RecipeCategorieData["definition"]["id"]
        self.isArchive = RecipeCategorieData["definition"]["isArchive"]
        self.isNoCraft = RecipeCategorieData["definition"]["isNoCraft"]
        self.isHidden = RecipeCategorieData["definition"]["isHidden"]
        self.xpFactor = RecipeCategorieData["definition"]["xpFactor"]
        self.isInnate = RecipeCategorieData["definition"]["isInnate"]
        try:
            self.title = Text(RecipeCategorieData["title"])
        except AttributeError:
            self.title = Text({
                "fr":"Type de recette Inconnu" if RecipeCategorieData["title"]["fr"] in [None, ""] else RecipeCategorieData["title"]["fr"],
                "en":"Unknow Recipe Categorie"
            })

    def __str__(self):
        return self.title.__str__()

    @classmethod
    async def asyncFindRecipeCategorie(cls, RecipeCategories:list, id:int):
        """
        Méthode asynchrone permettant de rechercher la catégorie
        de recette avec l'id donnée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeCategorie, (RecipeCategories, id))

    @staticmethod
    def findRecipeCategorie(RecipeCategories:list, id:int):
        """
        Méthode permettant de rechercher la catégorie de recette
        avec l'id donnée au sein des données d'Ankama.
        """
        search = Dichot(RecipeCategories, id, ['definition','id'])
        return RecipeCategorie(search) if search else None

    @classmethod
    async def asyncFindRecipeCategorieByName(cls, RecipeCategories:list, name:str, lang:str):
        """
        Méthode asynchrone permettant de rechercher la catégorie de recette
        avec le nom donné dans la langue donnée au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipeCategoriesByName, (RecipeCategories, name, lang))

    @classmethod
    def findRecipeCategoriesByName(cls, RecipeCategories:list, name:str, lang:str):
        """
        Méthode permettant de rechercher la catégorie de recette
        avec le nom donné dans la langue donnée au sein des données d'Ankama.
        """
        name = name.strip().lower()
        names = []
        threads = []
        for cat in RecipeCategories:
            cate = RecipeCategorie(cat)
            cls._defineThread(cls.processForName, (cate, name, lang, names), threads)
        cls._waitingThreads(threads)
        return names

    @staticmethod
    def processForName(cat, name:str, lang:str, names:list):
        if name == cat.title.getByShortLang(lang).lower().strip():
            names.append(cat)