class ServerElement:

    compt = 0

    def __init__(self, name:str, id:int = None, active:bool = True, redirectTo = None):
        self.name = name
        if id:
            self.id = id
        else:
            self.id = self.__class__.compt
            self.__class__.compt += 1
        self.active = active
        if self.active is None and (redirectTo is None or isinstance(redirectTo, self.__class__) == False):
            raise Exception("If server isn't active you must specify a redirection")
        self.redirectTo = redirectTo

    def disable(self, redirectTo):
        if self.active is False:
            raise Exception("Server is already disabled !")
        if redirectTo is None or isinstance(redirectTo, self.__class__) == False:
            raise Exception("If server isn't active you must specify a redirection")
        self.redirectTo = redirectTo
        self.active = False