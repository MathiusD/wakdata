from ..Servers import ServerElement

dathura = ServerElement("dathura")
aerafal = ServerElement("aerafal")
remington = ServerElement("remington")
elbor = ServerElement("elbor")
nox = ServerElement("nox")
efrim = ServerElement("efrim")
phaeris = ServerElement("phaeris")
pandora = ServerElement("pandora")
rubilax = ServerElement("rubilax")
ogrest_fr_1 = ServerElement("ogrest_fr_1")
ogrest_fr_2 = ServerElement("ogrest_fr_2")
ogrest_int = ServerElement("ogrest_int")
ogrest_fr_3 = ServerElement("ogrest_fr_3")
ogrest_fr_4 = ServerElement("ogrest_fr_4")
dathura.disable(pandora)
aerafal.disable(pandora)
remington.disable(rubilax)
elbor.disable(rubilax)
nox.disable(rubilax)
efrim.disable(rubilax)
phaeris.disable(rubilax)

class Server:

    servers = [
        dathura,
        aerafal,
        remington,
        elbor,
        nox,
        efrim,
        phaeris,
        pandora,
        rubilax,
        ogrest_fr_1,
        ogrest_fr_2,
        ogrest_int,
        ogrest_fr_3,
        ogrest_fr_4,
    ]

    @classmethod
    def serversAvailable(cls):
        servers = []
        for server in cls.servers:
            if server.active is True:
                servers.append(server)
        return servers

    @classmethod
    def indexOfServer(cls, server_name:str):
        if isinstance(server_name, ServerElement):
            server_name = server_name.name
        server_name = server_name.lower().strip()
        for server in cls.servers:
            if server.name == server_name:
                if server.active:
                    return cls.servers.index(server)
                elif server.redirectTo in cls.servers:
                    return cls.servers.index(server.redirectTo)
        return None

    @classmethod
    def indexOfServerElement(cls, server:ServerElement):
        return cls.servers.index(server) if server in cls.servers else None

    @classmethod
    def extractServerName(cls, server:int):
        serverData = cls.extractServer(server)
        return serverData.name if serverData is not None else None

    @classmethod
    def extractServer(cls, server:int):
        if len(cls.servers) > server >= 0:
            if cls.servers[server].active:
                return cls.servers[server]
            elif cls.servers[server].redirectTo in cls.servers:
                return cls.servers[server].redirectTo
        return None

    @classmethod
    def extractServersRedirectTo(cls, server:ServerElement):
        if server in cls.servers:
            out = []
            for temp in cls.servers:
                if temp.active == False and temp.redirectTo == server:
                    out.append(temp)
            return out
        return None