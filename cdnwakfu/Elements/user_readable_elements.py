from ..Utils import sheet_process
from ..Elements import DataElement

class UserReadableElement(DataElement):
    """
    Classe contenant des méthodes d'affichage
    de ses attributs.
    """

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 1):
        """
        Méthode permettant de générer le dictionnaire
        générant l'affichage de l'objet dans la langue donnée,
        avec un préfix spécifié et avec la profondeur souhaitée.
        """
        self._recursion(recursive)
        self._row = []
        self._lang = lang.lower().strip()
        return {"data":self._row,"prefix":""}

    def sheet(self, lang:str = "fr", prefix:str = ""):
        """
        Méthode permettant de générer l'affichage de l'objet
        dans la langue donnée et avec un préfix spécifié.
        """
        return sheet_process(self._row_sheet(lang, prefix))
