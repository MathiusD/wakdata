from threading import Thread
from concurrent.futures.thread import ThreadPoolExecutor
import asyncio

class SearchElement:
    """
    Classe représentant un élément permettant
    des recherches au sein de liste.\n
    Cette classe défini donc de nombreuses méthodes
    afin de faciliter la recherche multi-threadée et/ou
    asynchrone.
    """

    @staticmethod
    def _waitingThreads(threads:list):
        """
        Cette méthode permet d'attendre que tout les
        threads de la liste se sont achevés.
        """
        for thread in threads:
            if isinstance(thread, Thread) and thread.is_alive():
                thread.join()
        return True

    @staticmethod
    def _defineThread(func, args:tuple, threads:list = None):
        """
        Cette méthode permet de déclarer un thread qui exectura
        la `func` ciblée avec les `args` ciblés et si une liste
        de threads est jointe alors le thread y est ajouté.
        """
        thread = Thread(target=func, args=args)
        thread.run()
        if threads:
            threads.append(thread)
        return thread

    @staticmethod
    async def asyncSearch(func, args:tuple, maxThreads:int=10):
        """
        Cette méthode permet d'exectuer la `func` ciblée avec les
        `args` ciblés au sein d'un ThreadPoolExecutor sur au maximum
        le nombre de threads spécifiés dans `maxThreads`.\n
        Cela permet donc d'avoir une version asynchrone d'une fonction
        donnée.
        """
        return await asyncio.get_event_loop().run_in_executor(ThreadPoolExecutor(maxThreads), func, *args)