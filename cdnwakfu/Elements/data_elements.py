class DataElement:
    """
    Classe contenant une méthode afin de
    faciliter la gestion des méthodes
    récursives.
    """

    def _recursion(self, recursive:int = None):
        """
        Méthode permettant de préparer la récursion
        d'une méthode récursive.
        """
        if recursive:
            if recursive > 0:
                self._recursive = recursive - 1
            else:
                self._recursive = None
        else:
            self._recursive = None