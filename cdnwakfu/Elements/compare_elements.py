import copy, abc

class CompareElement:
    """
    Classe représentant un élément qui
    est comparable avec un autre élément
    de la même classe.
    """

    def __init__(self):
        self._comparedFrom = None
        self._comparedTo = None

    def _hasCompared(self, fromElement, toElement):
        """
        Méthode spécifiant que l'objet de comparaison
        est tiré de `fromElement` et a été comparé à
        `toElement`.
        """
        self._comparedFrom = fromElement
        self._comparedTo = toElement

    def compareTo(self, element, data=None):
        """
        Méthode permettant de comparer l'élément
        courant avec l'élément ciblé.
        """
        if self.__class__ == element.__class__:
            compare = copy.deepcopy(self)
            compare._hasCompared(self, element)
            return compare
        return None

    @abc.abstractmethod
    def _reverted(self):
        """
        Méthode permettant d'inverses les valeurs
        de l'élément courant.
        """
        pass

    def compareFrom(self, element, data=None):
        if self.__class__ == element.__class__:
            return element.compareTo(self, [data[1], data[0]] if data else None)
        return None

    def _compareReal(self, obj ,attr:str, other):
        setattr(obj, attr, getattr(other, attr) - getattr(self, attr))
        return obj
    
    def _reverted(self):
        return copy.deepcopy(self)

    def _compareListAttr(self, obj ,attr:str, other, data:list = [None, None]):
        setattr(obj, attr, [])
        for typ in getattr(self, attr):
            temp = None
            for ty in getattr(other, attr):
                if isinstance(typ, CompareElement) and isinstance(ty, CompareElement):
                    temp = typ.compareTo(ty, data)
                    if temp:
                        break
            if temp:
                appd = temp
            else:
                appd = typ._reverted()
            lis = getattr(obj, attr)
            lis.append(appd)
            setattr(obj, attr, lis)
        for typ in getattr(other, attr):
            temp = None
            for ty in getattr(self, attr):
                if isinstance(typ, CompareElement) and isinstance(ty, CompareElement):
                    temp = typ.compareTo(ty, [data[1], data[0]])
                    if temp:
                        break
            if not temp:
                lis = getattr(obj, attr)
                lis.append(typ)
                setattr(obj, attr, lis)
        return obj