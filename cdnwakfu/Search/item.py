from ..Items import Item
from ..JobItems import JobItem
from ..Elements import SearchElement

class SearchItem(SearchElement):
    """
    Classe contenant diverses méthodes de recherches
    d'Objets que ces derniers soit des JobItems ou
    des Items.
    """

    @staticmethod
    def getItemsByName(GameData:dict, name:str, lang:str):
        """
        Méthode permettant de rechercher tout les Items et JobItems
        correspondant au nom dans la langue donnée.
        """
        items = []
        ites = None
        if "items" in GameData.keys():
            ites = Item.findItemsByName(GameData["items"], name, lang)
            for item in ites:
                items.append(item)
        if "jobsItems" in GameData.keys():
            jobs = JobItem.findItemsByName(GameData["jobsItems"], name, lang)
            for item in jobs:
                push = True
                if ites:
                    for ite in ites:
                        if ite.definition.item.id == item.definition.id:
                            push = False
                            break
                if push == True:
                    items.append(item)
        return items

    @classmethod
    async def asyncGetItemsByName(cls, GameData:dict, name:str, lang:str):
        """
        Méthode asynchrone permettant de rechercher tout les Items
        et JobItems correspondant au nom dans la langue donnée.
        """
        return await cls.asyncSearch(cls.getItemsByName, (GameData, name, lang))

    @staticmethod
    def getItem(GameData:dict, id:int):
        """
        Méthode permettant de rechercher l'objet correspondant
        à l'id donné que ce dernier soit un Item ou un JobItem.
        """
        item = None
        if "items" in GameData.keys():
            item = Item.findItem(GameData["items"], id)
        if "jobsItems" in GameData.keys() and (not item):
            item = JobItem.findItem(GameData["jobsItems"], id)
        return item

    @classmethod
    async def asyncGetItemsById(cls, GameData:dict, id:int):
        """
        Méthode asynchrone permettant de rechercher l'objet correspondant
        à l'id donné que ce dernier soit un Item ou un JobItem.
        """
        return await cls.asyncSearch(cls.getItem, (GameData, id))