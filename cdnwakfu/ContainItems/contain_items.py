from ..Elements import UserReadableElement, CompareElement, SearchElement
from ..Texts import Text
from ..Items import Item
from ..JobItems import JobItem
from ..Search import SearchItem

class ContainItem(UserReadableElement, CompareElement, SearchElement):
    """
    Classe représentant une donnée qui
    contient un équipement.
    """

    def __init__(self, itemId:int, quantity:int, id:int = None):
        super().__init__()
        self.id = id
        self._itemId = itemId
        self.item = None
        self._quantity = quantity

    def fetchData(self, GameData:dict, recursive:int = 1):
        super()._recursion(recursive)
        if not self.item:
            self.item = SearchItem.getItem(GameData, self._itemId)
        if self.item and self._recursive:
            self.item.fetchData(GameData, self._recursive)

    def compareTo(self, containItem, data=None):
        compare = super().compareTo(containItem)
        if compare and self._itemId == containItem._itemId:
            self._compareReal(compare, "_quantity", containItem)
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        for attr in ["_quantity"]:
            setattr(reverted, attr, -1 * getattr(reverted, attr))
        return reverted

    def _row_sheet(self, lang:str = "fr", prefix:str = "", recursive:int = 6):
        super()._row_sheet(lang, prefix, recursive)
        if self.item:
            line = "%i * %s" % (self._quantity, self.item.title.getByShortLang(self._lang)) if self.item.title else "%i" % self._quantity
            line = "%s :" % line if self._recursive else line
            self._row.append(line)
            if self._recursive:
                self._row.append(self.item._row_sheet(self._lang, "\t", self._recursive))
        return {"data":self._row,"prefix":prefix}