from ..Utils import Dichot
from ..Elements import SearchElement

class ItemPropertie(SearchElement):
    """
    Classe représentant la propriété d'un Item
    dans les données d'Ankama.
    """

    def __init__(self, ItemPropertieData:dict):
        self.id = ItemPropertieData["id"]
        self.name = ItemPropertieData["name"]
        self.description = ItemPropertieData["description"]

    def __str__(self):
        return self.name

    @staticmethod
    def findPropertie(properties:list, id:int):
        """
        Méthode permettant de trouver la propriété avec
        l'id spécifié au sein des données d'Ankama.
        """
        search = Dichot(properties, id, ['id'])
        return ItemPropertie(search) if search else None

    @classmethod
    async def asyncFindPropertie(cls, properties:list, id:int):
        """
        Méthode asynchrone permettant de trouver la propriété avec
        l'id spécifié au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findPropertie, (properties, id))