from ..Utils import Dichot
from ..Elements import DataElement, CompareElement
from ..CollectibleResources import CollectibleResource

class HarvestLoot(DataElement, CompareElement):
    """
    Classe représentant les taux d'obtention
    d'Objet suite à une action avec une Ressource.
    """

    def __init__(self, HarvestLootData:dict):
        super().__init__()
        self.id = HarvestLootData["id"]
        self.itemId = HarvestLootData["itemId"]
        self.quantity = HarvestLootData["quantity"]
        self.requiredProspection = HarvestLootData["requiredProspection"]
        self.dropRate = HarvestLootData["dropRate"]
        self.listId = HarvestLootData["listId"]
        self.quantityPerItem = HarvestLootData["quantityPerItem"]
        self.quantityMin = HarvestLootData["quantityMin"]
        self.quantityMax = HarvestLootData["quantityMax"]
        self.maxRoll = HarvestLootData["maxRoll"]
        self.itemIsLootList = HarvestLootData["itemIsLootList"]
        self.collectibleResources = []

    def fetchData(self, GameData:dict, recursive:int = 3):
        super()._recursion(recursive)
        if "collectibleResources" in GameData.keys() and len(self.collectibleResources) == 0:
            self.collectibleResources = CollectibleResource.findCollectibleResources(GameData["collectibleResources"], self.itemId)
        if self._recursive:
            for collectibleResource in self.collectibleResources:
                collectibleResource.fetchData(GameData, self._recursive)

    def __str__(self):
        return str(self.id)

    def compareTo(self, harvestLoot, data=None):
        compare = super().compareTo(harvestLoot)
        if compare and self.itemId == harvestLoot.itemId:
            for attr in ["quantity", "requiredProspection", "dropRate", "quantityPerItem", "quantityMin", "quantityMax", "maxRoll"]:
                compare = self._compareReal(compare, attr, harvestLoot)
            return compare
        return None

    def _reverted(self):
        reverted = super()._reverted()
        for attr in ["quantity", "requiredProspection", "dropRate", "quantityPerItem", "quantityMin", "quantityMax", "maxRoll"]:
            setattr(reverted, attr, -1 * getattr(reverted, attr))
        return reverted

    @staticmethod
    def findHarvestLoots(harvestLoots:dict, id:int):
        """
        Méthode pour trouver tout les loots permettant
        d'obtenir l'item avec l'id spécifiés
        """
        loots = {}
        for loot in harvestLoots:
            if loot['itemId'] == id:
                if not (loot["listId"] in loots.keys()):
                    loots[loot["listId"]] = []
                loots[loot["listId"]].append(HarvestLoot(loot))
        lootlists = []
        for lootlist in loots:
            lootlists.append(loots[lootlist])
        return lootlists
