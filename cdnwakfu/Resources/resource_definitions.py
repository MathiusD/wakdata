from ..ResourceTypes import ResourceType

class ResourceDefinition:
    """
    Classe représentant différentes données
    liées à une ressource.
    """

    def __init__(self, ResourceDefinitionData:dict):
        self.id = ResourceDefinitionData["id"]
        self._resourceType = ResourceDefinitionData["resourceType"]
        self.isBlocking = ResourceDefinitionData["isBlocking"]
        self.idealRainRangeMin = ResourceDefinitionData["idealRainRangeMin"]
        self.idealRainRangeMax = ResourceDefinitionData["idealRainRangeMax"]
        self.idealTemperatureRangeMin = ResourceDefinitionData["idealTemperatureRangeMin"]
        self.idealTemperatureRangeMax = ResourceDefinitionData["idealTemperatureRangeMax"]
        self.iconGfxId = ResourceDefinitionData["iconGfxId"]
        self.lastEvolutionStep = ResourceDefinitionData["lastEvolutionStep"]
        self.usableByHeroes = ResourceDefinitionData["usableByHeroes"]
        self.idealRain = ResourceDefinitionData["idealRain"]
        self.resourceType = None

    @property
    def vertyloUri(self):
        """
        Propriété menant vers l'asset de la ressource sur
        le dépôt de vertylo.
        """
        return "https://vertylo.github.io/wakassets/items/%s.png" % self.iconGfxId

    @property
    def uri(self):
        """
        Propriété menant vers l'asset de la ressource sur
        les serveurs d'Ankama.
        """
        return self.constructUri()

    def constructUri(self, size:int=115):
        """
        Propriété menant vers l'asset de la ressource sur
        les serveurs d'Ankama avec la taille ciblée.
        """
        return "https://s.ankama.com/www/static.ankama.com/wakfu/portal/game/item/%i/%s.png" % (size, self.iconGfxId)

    def fetchData(self, GameData:dict):
        if "resourceTypes" in GameData.keys() and not self.resourceType:
            self.resourceType = ResourceType.findResourceType(GameData["resourceTypes"], self._resourceType)