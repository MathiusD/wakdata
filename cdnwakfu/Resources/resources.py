from ..Utils import Dichot
from ..Elements import DataElement, SearchElement
from ..Resources import ResourceDefinition
from ..Texts import Text

class Resource(DataElement, SearchElement):
    """
    Classe représentant une ressource
    au sein des données d'Ankama.
    """

    def __init__(self, ResourceData:dict):
        self.definition = ResourceDefinition(ResourceData["definition"])
        try:
            self.title = Text(ResourceData["title"])
        except AttributeError:
            self.title = Text({
                "fr":"Ressource Inconnue" if ResourceData["title"]["fr"] in [None, ""] else ResourceData["title"]["fr"],
                "en":"Unknow Resource"
            })

    def fetchData(self, GameData:dict, recursive:int = 2):
        super()._recursion(recursive)
        if self._recursive:
            self.definition.fetchData(GameData)

    def __str__(self):
        return self.title.__str__()

    @classmethod
    async def asyncFindResource(cls, resource:list, id:int):
        """
        Méthode asynchrone permettant de rechercher de la ressource
        donnée avec l'id donné au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findResource, (resource, id))

    @staticmethod
    def findResource(resource:list, id:int):
        """
        Méthode permettant de rechercher de la ressource
        donnée avec l'id donné au sein des données d'Ankama.
        """
        search = Dichot(resource, id, ['definition', 'id'])
        return Resource(search) if search else None