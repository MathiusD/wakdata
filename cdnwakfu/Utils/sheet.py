def sheet_process(data_row:dict):
    out = ""
    for row in data_row["data"]:
        if isinstance(row, dict):
            newrow = row
            newrow["prefix"] = "%s%s" % (data_row["prefix"], row["prefix"])
            out = "%s%s" % (out, sheet_process(newrow))
        else:
            out = "%s%s%s\n" % (out, data_row["prefix"], row)
    return out