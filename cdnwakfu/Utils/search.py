def AccessData(dic:list or dict, access:list):
    for key in access:
        dic = dic[key]
    return dic
        

def Dichot(dic:list, value:object, access:list):
    """
    Méthode de recherche dichotomique au sein d'une
    liste sur des attributs au sein de dictionnaire
    permettant donc de comparer des attributs précis
    d'un dictionnaire.\n
    Exemple :
    ```py
    >>> Dichot([{"id":0}, {"id":1}], 1, ["id"])
    >>> {"id":1}
    ```
    L'exemple ci-dessous permet donc de chercher un
    id valant 1 dans la liste de dictionnaire.
    """
    if len(dic) > 0:
        start = 0
        end = len(dic) - 1
        if len(dic) > 1:
            mid = int((start + end) /2)
            mid_value = AccessData(dic[mid], access)
            iterate = 0
            while iterate < 4:
                if mid_value == value:
                    return dic[mid]
                elif mid_value > value:
                    end = mid
                else:
                    start = mid
                mid = int((start + end) /2)
                mid_value = AccessData(dic[mid], access)
                start_value = AccessData(dic[start], access)
                end_value = AccessData(dic[end], access)
                if iterate == 1:
                    mid += 1
                if mid_value == end_value or mid_value == start_value:
                    iterate += 1
                else:
                    iterate = 0
        else:
            if AccessData(dic[start], access) == value:
                return dic[start]
            elif AccessData(dic[end], access) == value:
                return dic[end]
    return None