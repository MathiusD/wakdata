import re
from ..Texts import Text

fire = Text({
    "fr":"Feu",
    "en":"Fire",
    "es":"Fuego",
    "pt":"Fogo"
})
water = Text({
    "fr":"Eau",
    "en":"Water",
    "es":"Agua",
    "pt":"Agua"
})
earth = Text({
    "fr":"Terre",
    "en":"Earth",
    "es":"Tierra",
    "pt":"Terra"
})
air = Text({
    "fr":"Air",
    "en":"Air",
    "es":"Aire",
    "pt":"Aria"
})
light = Text({
    "fr":"Lumière",
    "en":"Light",
    "es":"Luz",
    "pt":"Luz"
})

class Data:

    def __init__(self, String:str):
        search = re.search("{\[([~\+\-])(\d)]\?([^{\}])*:([^{\}])*}", String)
        self.conditional_data = search.group(0) if search else None
        search = re.search("\[#\d](^\[#\d])*{\[[>\<\=]\d]\?s?:}", String)
        self.simple_ternary_data = search.group(0) if search else None
        search = re.search("{\[\d*[>\<\=]\d]\?([^{\}])*:([^{\}])*}", String)
        self.complex_ternary_data = search.group(0) if search else None
        search = re.search("\|.*\|", String)
        self.compute_data = search.group(0) if search else None
        search = re.search("\[#\d]", String)
        self.parameters_data = search.group(0) if search else None
        
class ElementData:

    def __init__(self, String:str):
        search = re.search("{\[([~\+\-])(\d)]\?([^{\}])*:([^{\}])*}", String)
        self.conditional_data = search.group(0) if search else None
        search = re.search("\[#\d](^\[#\d])*{\[[>\<\=]\d]\?s?:}", String)
        self.simple_ternary_data = search.group(0) if search else None
        search = re.search("{\[\d*[>\<\=]\d]\?([^{\}])*:([^{\}])*}", String)
        self.complex_ternary_data = search.group(0) if search else None
        search = re.search("\|.*\|", String)
        self.compute_data = search.group(0) if search else None
        search = re.search("\[#\d]", String)
        self.parameters_data = search.group(0) if search else None

class OperatorStr:

    def __init__(self, operator:str):
        self.operator = operator

    def process(self, fvalue, svalue):
        if self.operator == "*" or self.operator == ".":
            return fvalue * svalue
        if self.operator == "/":
            return fvalue / svalue
        if self.operator == "+":
            return fvalue + svalue
        if self.operator == "-":
            return fvalue - svalue
        return fvalue

class ComparatorStr:

    def __init__(self, operator:str = "="):
        operator = operator if operator in [">", "<", "=", ">=", "<="] else "="
        self.operator = operator

    def process(self, fvalue, svalue):
        if self.operator == ">":
            return True if fvalue > svalue else False
        if self.operator == "<":
            return True if fvalue < svalue else False
        if self.operator == "=":
            return True if fvalue == svalue else False
        if self.operator == ">=":
            return True if fvalue >= svalue else False
        if self.operator == "<=":
            return True if fvalue <= svalue else False

class Analyse:

    @classmethod
    def analyseDescription(cls, descriptionAction:str, paramsEffect:list, lvl:int):
        return cls._Analyse(descriptionAction, paramsEffect, lvl).strip()

    @staticmethod
    def defineElements(effect:str, lang:str):
        effect = effect.replace("[el1]", fire.getByShortLang(lang))
        effect = effect.replace("[el2]", water.getByShortLang(lang))
        effect = effect.replace("[el3]", earth.getByShortLang(lang))
        effect = effect.replace("[el4]", air.getByShortLang(lang))
        effect = effect.replace("[el6]", light.getByShortLang(lang))
        return effect

    @classmethod
    def _Analyse(cls, descriptionAction:str, paramsEffect:list, lvl:int):
        data = Data(descriptionAction)
        params = None
        while data.conditional_data or data.simple_ternary_data or data.complex_ternary_data or data.compute_data or data.parameters_data:
            data = Data(descriptionAction)
            if data.conditional_data:
                descriptionAction = descriptionAction.replace(data.conditional_data, cls._conditional(data.conditional_data, paramsEffect, lvl))
            elif data.complex_ternary_data:
                descriptionAction = descriptionAction.replace(data.complex_ternary_data, cls._complex_ternary(data.complex_ternary_data, paramsEffect, lvl))
            elif data.simple_ternary_data:
                descriptionAction = descriptionAction.replace(data.simple_ternary_data, cls._simple_ternary(data.simple_ternary_data, paramsEffect, lvl, params))
            elif data.compute_data:
                descriptionAction = descriptionAction.replace(data.compute_data, cls._compute(data.compute_data, paramsEffect, lvl))
            elif data.parameters_data:
                descriptionAction = descriptionAction.replace(data.parameters_data, cls._parameters(data.parameters_data, paramsEffect, lvl))
        return descriptionAction

    @classmethod
    def _parameters(cls, extractString:str, paramsEffect:list, lvl:int):
        index = int(extractString[2])
        return str(round(cls._calc_value(index, paramsEffect, lvl)))

    @staticmethod
    def _calc_value(index:int, paramsEffect:list, lvl:int):
        if ((index-1)*2) + 1 < len(paramsEffect):
            index -= 1
            return paramsEffect[index*2 + 1] * lvl + paramsEffect[index*2]
        return 0

    @classmethod
    def _complex_ternary(cls, extractString:str, paramsEffect:list, lvl:int):
        cond = extractString.split("?")[0][1:]
        out = extractString[len(cond)+2:-1]
        index = int(cond[len(cond)-2])
        oper = OperatorStr(cond[len(cond)-3])
        value = int(cond[1:len(cond)-4]) if cond[1:len(cond)-4] != "" else 0
        exact = out[:len(out.split(":")[0])]
        inexact = out[len(out.split(":")[0])+1:]
        param = cls._calc_value(index, paramsEffect, lvl)
        return exact if oper.process(param, value) else inexact

    @classmethod
    def _simple_ternary(cls, extractString:str, paramsEffect:list, lvl:int):
        params = cls._calc_value(int(re.search(extractString, "\[#\d]").group(0)[2]), paramsEffect, lvl)
        extractString = re.search(extractString, "{\[[>\<\=]\d]\?s?:}").group(0)
        value = int(extractString[3])
        exact = extractString[6:len(extractString.split(":")[0])]
        inexact = extractString[-len(extractString.split(":")[1]):-1]
        oper = OperatorStr(extractString[2])
        return exact if oper.process(params, value) else inexact

    @classmethod
    def _compute(cls, extractString:str, paramsEffect:list, lvl:int):
        fvalue = cls._calc_value(int(extractString[3]), paramsEffect, lvl)
        svalue = cls._calc_value(int(extractString[5]), paramsEffect, lvl)
        operator = OperatorStr(extractString[4])
        value = operator.process(fvalue, svalue)
        operator = OperatorStr(extractString.split(']')[1][0])
        const = int(extractString.split(']')[1][1:-1])
        return str(int(operator.process(value, const)))

    @staticmethod
    def _conditional(extractString:str, paramsEffect:list, lvl:int):
        operator = extractString[2]
        cond = int(extractString[3])
        condstr = extractString.split("?")[0][1:]
        out = extractString[len(condstr)+2:-1]
        exact = out[:len(out.split(":")[0])]
        inexact = out[len(out.split(":")[0])+1:]
        if operator == "~":
            return exact if len(paramsEffect)/2 == cond else inexact
        if operator == "+":
            return exact if len(paramsEffect)/2 > cond else inexact
        if operator == "-":
            return exact if len(paramsEffect)/2 < cond else inexact