from ..Texts import Text
from ..Utils import Dichot
from ..Elements import SearchElement

class RecipePattern(SearchElement):
    """
    Classe représentant le plan d'une recette
    au sein des données d'Ankama (Archive FanMade).
    """

    def __init__(self, RecipePatternData:dict):
        self.id = RecipePatternData["id"]
        self.recipeId = RecipePatternData["recipeId"]
        dictName = RecipePatternData["name"]
        try:
            self.name = Text(dictName)
        except AttributeError:
            self.name = Text({
                "fr":"Plan Inconnu" if dictName is None or dictName["fr"] in [None, ""] else dictName["fr"],
                "en":"Unknow Recipe Pattern" if dictName is None or dictName["en"] in [None, ""] else dictName["en"]
            })
        dictObtention = RecipePatternData["obtention"]
        try:
            self.obtention = Text(dictObtention)
        except AttributeError:
            self.obtention = Text({
                "fr":"Moyen d'Obtention Inconnu" if dictObtention is None or dictObtention["fr"] in [None, ""] else dictObtention["fr"],
                "en":"Unknow Obtention" if dictObtention is None or dictObtention["en"] in [None, ""] else dictObtention["en"]
            })

    def __str__(self):
        return self.name.__str__()

    @classmethod
    async def asyncFindRecipePattern(cls, recipePattern:list, id:int):
        """
        Méthode asynchrone permettant de rechercher le plan avec l'id
        données au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipePattern, (recipePattern, id))

    @staticmethod
    def findRecipePattern(recipePattern:list, id:int):
        """
        Méthode permettant de rechercher le plan
        avec l'id donné au sein des données d'Ankama.
        """
        search = Dichot(recipePattern, id, ['id'])
        return RecipePattern(search) if search else None

    @classmethod
    async def asyncFindRecipePatternByRecipe(cls, recipePattern:list, recipeId:int):
        """
        Méthode asynchrone permettant de rechercher le plan de
        la recette avec l'id donné au sein des données d'Ankama.
        """
        return await cls.asyncSearch(cls.findRecipePatternByRecipe, (recipePattern, recipeId))

    @staticmethod
    def findRecipePatternByRecipe(recipePattern:list, recipeId:int):
        """
        Méthode permettant de rechercher le plan de
        la recette avec l'id donné au sein des données d'Ankama.
        """
        for pattern in recipePattern:
            if recipeId in pattern["recipeId"]:
                return RecipePattern(pattern)
        return None