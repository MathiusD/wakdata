import os, shutil
from config_wakdata import PATH_DATA
from own_install import install

install(["json"], "own", "MathiusD", "gitlab.com")

if not os.path.exists(PATH_DATA):
    os.mkdir(PATH_DATA)
shutil.copyfile('building/__init_data__.py', '%s/__init__.py' % (PATH_DATA))