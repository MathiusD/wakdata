from cdnwakfu import Craftkfu
from config_wakdata import settings
from data import latest

craftkfuData = Craftkfu.writeCraftkfuData(latest, "%s/%s/craftkfu.json" % (settings.PATH_DATA, latest["version"]))

import json
from os.path import exists

buildingVersion = "building/version.json"
if buildingVersion and exists(buildingVersion):
    with open(buildingVersion, 'r') as buildingVersionFile:
        version = json.load(buildingVersionFile)
        if version["version"] == latest["version"]:
            Craftkfu.writeInFile(craftkfuData, "building/craftkfu.json")